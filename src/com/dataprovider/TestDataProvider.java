package com.dataprovider;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.DataProvider;

import com.utility.CSVUtility;

public class TestDataProvider {
	private static Object[][] obj;
	@SuppressWarnings("rawtypes")
	private static List parList=new ArrayList();
    private static List<Map<String, String>> sonList=new ArrayList<Map<String, String>>();

    @DataProvider()
    public static Object[][] dataProvider(Method method) {
        System.out.println("@DataProvider(name='dataProvider')");
        System.out.println("dataProvider" + method.getName());
        
        parList = CSVUtility.getCsvElements("testdata/TestData.csv");
        sonList.clear();

        for (int i=0;i< parList.size();i++) {
        	@SuppressWarnings("rawtypes")
            Map map = (Map)parList.get(i);
            if (map.containsKey(method.getName())) {
                @SuppressWarnings("unchecked")
				Map<String,String> submap = (Map<String,String>) map.get(method.getName());
                sonList.add(submap);

            }
        }

        if (sonList.size() > 0) {

            obj = new Object[sonList.size()][];
            for (int i = 0; i < sonList.size(); i++) {
                obj[i] = new Object[] { sonList.get(i) };
            }          
            return obj;
        }else{
            Assert.assertTrue(sonList.size()!=0,parList + "is null. Can't find " + method.getName() );
            return null;
        }

    }
}
