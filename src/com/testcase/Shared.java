package com.testcase;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.pageobject.CheckAvailability;
import com.pageobject.SignIn;

public class Shared {

	public static void SignIn(WebDriver driver, String username, String password) throws Exception{
		SignIn signin = new SignIn(driver);
		
		Actions action = new Actions(driver);
		
		if(!signin.getSignInButton().isDisplayed()) {
			signin.getSignIn().click();
			Thread.sleep(1000);
		}
		Thread.sleep(1000);
		action.sendKeys(signin.getUsername(), username).perform();
		Thread.sleep(1000);
		action.sendKeys(signin.getPassword(), password).perform();
		Thread.sleep(1000);
		signin.getRememberMe().click();
		Thread.sleep(1000);
		signin.getSignInButton().click();
		Thread.sleep(20000);
		signin.getSignInUser().click();
		Thread.sleep(2000);
	}
	
	public static void SignOut(WebDriver driver) throws Exception{
		SignIn signin = new SignIn(driver);
		
		Actions action = new Actions(driver);
		
		action.click(signin.getSignOut()).perform();
		Thread.sleep(5000);
		if(!driver.getClass().getName().toLowerCase().contains("chrome")){
			Alert alt = driver.switchTo().alert();
			alt.accept();
			Thread.sleep(10000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
		}
	}
	
	public static void SearchAvailableHotel(WebDriver driver, String keyword) throws Exception{
		CheckAvailability checkavailability = new CheckAvailability(driver);
		
		Actions action = new Actions(driver);
		
		action.click(checkavailability.getSearchBox()).perform();
		Thread.sleep(1000);
		action.sendKeys(checkavailability.getSearchBox(),keyword).perform();
		Thread.sleep(10000);
		action.click(checkavailability.getSearchListItems().get(checkavailability.getSearchListItems().size()-1)).perform();
		action.click(checkavailability.getCheckIn()).perform();
		Thread.sleep(1000);	
		for(int i=0;i<7;i++)
			action.click(checkavailability.getDatePickerNext()).perform();
		Thread.sleep(1000);
		action.click(checkavailability.getDateItems().get(0)).perform();
		Thread.sleep(3000);	
		action.click(checkavailability.getCheckOut()).perform();
		Thread.sleep(1000);
		action.click(checkavailability.getDateItems().get(1)).perform();
		Thread.sleep(3000);
		action.click(checkavailability.getCheckAvailabilityButton()).perform();
	}

}
