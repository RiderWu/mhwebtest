package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.web.Common;
import com.pageobject.CheckAvailability;
import com.pageobject.Feedback;
import com.pageobject.Footer;
import com.pageobject.HomePage;
import com.pageobject.MyMillennium_Profile;
import com.pageobject.MyMillennium_Scratchpad;
import com.pageobject.MyMillennium_SignUp;
import com.pageobject.MyMillennium_TravelPreferences;
import com.pageobject.Navigation;
import com.pageobject.Privacy;
import com.pageobject.ScrollButtons;
import com.pageobject.Search;
import com.pageobject.SignIn;

public class Home_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageLoad(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HomePageLoad");
			

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageNavigation(Map<?, ?> param) throws Exception {
		try {
			Navigation navigation = new Navigation(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.clickAndHold(navigation.getLogo()).perform();
			Thread.sleep(3000);			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-MouseOverLogo");
			action.release().perform();
			
			action.clickAndHold(navigation.getHotels()).perform();
			Thread.sleep(3000);			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-MouseOverHotels");
			action.release().perform();
			
			navigation.getLogo().click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-ClickLogo");
			
			action.click(navigation.getHotels()).perform();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ClickHotels");
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(navigation.getDestinations()).perform();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "05-ClickDestinations");
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(navigation.getOffers()).perform();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "06-ClickOffers");
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(navigation.getTours()).perform();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "07-ClickTours");
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(navigation.getWeddings()).perform();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "08-ClickWeddings");
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(navigation.getMeetings()).perform();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "09-ClickMeetings");
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(navigation.getBlog()).perform();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "10-ClickBlog");
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageSearch(Map<?, ?> param) throws Exception {
		try {
			
			Search search = new Search(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(search.getSearch()).perform();
			Thread.sleep(10000);			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickSearch");
			
			action.sendKeys("Mayfair").perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-InputSearchValidKeyword");
			
			for(int i=0;i<10;i++)
				action.sendKeys(Keys.BACK_SPACE);
				
			action.sendKeys("abc").perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-InputSearchInvalidKeyword");
			
			for(int i=0;i<10;i++)
				action.sendKeys(Keys.BACK_SPACE);
			
			action.click(search.getClose()).perform();
			Thread.sleep(1000);	
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ClickClose");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageSignIn(Map<?, ?> param) throws Exception {
		try {
			String ScreenshotName = null;
			
			SignIn signin = new SignIn(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(signin.getAvatar()).perform();
			action.click(signin.getAvatar()).perform();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickAvatar");
			
			action.click(signin.getSignIn()).perform();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-ClickSignIn");
			
			action.click(signin.getSignOnFacebook()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-ClickSignOnWithFacebook");
			
			driver.navigate().back();
			action.click(signin.getSignIn()).perform();
			
			action.click(signin.getSignOnGoogle()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ClickSignOnWithGoogle");
			
			driver.navigate().back();
			action.click(signin.getSignIn()).perform();
			
			signin.getRegisterNow().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "05-ClickRegisterNow");
			
			MyMillennium_SignUp signup = new MyMillennium_SignUp(driver);
			Common.scrollTo(signup.getRegisterNow());
			signup.getRegisterNow().click();
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "06-SignOnValidation");
			
			driver.navigate().back();
			action.click(signin.getSignIn()).perform();
			
			action.click(signin.getForgetPassword()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "07-ClickForgetPassword");
			
			driver.navigate().back();
			action.click(signin.getSignIn()).perform();
			
			action.sendKeys(signin.getUsername(), "Rider.Wu@mullenloweprofero.com");
			action.sendKeys(signin.getPassword(), "Profero@123");
			action.click(signin.getRememberMe()).perform();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "08-InputValidAccount");
			
			action.click(signin.getSignInButton()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "09-SignInWithValidAccount");
			
			action.click(signin.getViewProfile()).perform();
			Thread.sleep(20000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "10-ClickViewProfile");
			
			MyMillennium_Scratchpad mspage = new MyMillennium_Scratchpad(driver);
			Common.scrollTo(0);
			Thread.sleep(1000);
			action.click(mspage.getManageMyAccount()).perform();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "11-ClickManageMyAccount");
			
			MyMillennium_Profile mppage = new MyMillennium_Profile(driver);
			for(int i=0; i<mppage.getManageItems().size(); i++) {
				ScreenshotName = mppage.getManageItems().get(i).getText();
				action.click(mppage.getManageItems().get(i)).perform();
				Common.scrollTo(0);
				Thread.sleep(1000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						 "12-"+ScreenshotName);
			}
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(mspage.getPointsHistory()).perform();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "13-ClickPointsHistory");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(mspage.getTravelPreferences()).perform();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "14-ClickTravelPreferences");
			
			MyMillennium_TravelPreferences mtpage = new MyMillennium_TravelPreferences(driver);
			for(int i=0; i<mtpage.getPreferencesItems().size(); i++) {
				ScreenshotName = mtpage.getPreferencesItems().get(i).getText();
				action.click(mtpage.getPreferencesItems().get(i)).perform();
				Common.scrollTo(0);
				Thread.sleep(1000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						 "15-"+ScreenshotName);
			}
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			driver.navigate().back();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "16-SignInStatus");
			
			action.click(signin.getSignOut()).perform();
			Thread.sleep(5000);
			if(!Browser.equals("Chrome")){
				Alert alt = driver.switchTo().alert();
				alt.accept();
				Thread.sleep(10000);
				driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			}
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "17-ClickSignOut");
			
			action.click(signin.getShoppingCart()).perform();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "18-ClickShoppingCart");
			
			action.click(signin.getCurrency()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "19-ClickCurrency");
			
			action.click(signin.getCurrency()).perform();
			
			action.click(signin.getLanguage()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "20-ClickLanguage");
			
			action.click(signin.getLanguage()).perform();
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageCheckAvailability(Map<?, ?> param) throws Exception {
		try {
			
			CheckAvailability checkavailability = new CheckAvailability(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(checkavailability.getSearchBox()).perform();
			Thread.sleep(5000);			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickCheckAvailabilityBox");
			
			action.sendKeys(checkavailability.getSearchBox(),"abc").perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-InputInvalidKeywordKeywordInCheckAvailabilityBox");
			
			for(int i=0;i<10;i++)
				action.sendKeys(Keys.BACK_SPACE);
			
			action.sendKeys(checkavailability.getSearchBox(),"Mayfair").perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-InputKeywordInCheckAvailabilityBox");
			
			action.click(checkavailability.getSearchListItems().get(checkavailability.getSearchListItems().size()-1)).perform();
			action.click(checkavailability.getCheckIn()).perform();
			Thread.sleep(1000);	
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ClickCheckIn");
			
			for(int i=0;i<7;i++)
				action.click(checkavailability.getDatePickerNext()).perform();
			Thread.sleep(1000);
			action.click(checkavailability.getDateItems().get(0)).perform();
			Thread.sleep(5000);	
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "05-ChooseCheckIn");
			
			action.click(checkavailability.getCheckOut()).perform();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "06-ClickCheckOut");
			
			action.click(checkavailability.getDateItems().get(1)).perform();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "07-ChooseCheckOut");
			
			action.click(checkavailability.getCheckAvailabilityButton()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "08-ClickCheckAvailabilityButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageFeedback(Map<?, ?> param) throws Exception {
		try {
			
			Feedback feedback = new Feedback(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(feedback.getFeedbackButton()).perform();
			Thread.sleep(10000);			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickFeedbackButton");
			
			/*
			action.click(feedback.getFeedbackClose()).perform();
			Thread.sleep(5000);			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "ClickFeedbackClose");
			*/
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePagePrivacy(Map<?, ?> param) throws Exception {
		try {
			
			Privacy privacy = new Privacy(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(privacy.getPrivacyLink()).perform();
			Thread.sleep(10000);			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickPrivacyLink");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageScrolling(Map<?, ?> param) throws Exception {
		try {
			
			ScrollButtons scrollbuttons = new ScrollButtons(driver);
			HomePage homepage = new HomePage(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(scrollbuttons.getScrollDown()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickScrollDownButton");
			
			Common.scrollTo((WebElement)homepage.getHalfReviews().toArray()[2]);
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-ScrollDownWithScrollBar");
			
			Common.scrollTo(20000);
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-ScrollDownToBottom");
			
			action.click(scrollbuttons.getBackToTop()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ClickBackToTopButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageScrollingOnNonEnglish(Map<?, ?> param) throws Exception {
		try {
			
			ScrollButtons scrollbuttons = new ScrollButtons(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(scrollbuttons.getScrollDown()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickScrollDownButton");
			
			action.click(scrollbuttons.getBackToTop()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-ClickBackToTopButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageCheckAvailabilityOnTop(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			ScrollButtons scrollbuttons = new ScrollButtons(driver);
			
			action.click(scrollbuttons.getScrollDown()).perform();
			Thread.sleep(5000);
			
			CheckAvailability checkavailability = new CheckAvailability(driver);
			
			action.click(checkavailability.getSearchBox()).perform();
			Thread.sleep(5000);	
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickCheckAvailabilityBox");
			
			action.sendKeys(checkavailability.getSearchBox(),"abc").perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-InputInvalidKeywordKeywordInCheckAvailabilityBox");
			
			for(int i=0;i<10;i++)
				action.sendKeys(Keys.BACK_SPACE);
			
			action.sendKeys(checkavailability.getSearchBox(),"Mayfair").perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-InputKeywordInCheckAvailabilityBox");
			
			action.click(checkavailability.getSearchListItems().get(checkavailability.getSearchListItems().size()-1)).perform();
			
			/*
			action.click(checkavailability.getCheckIn()).perform();
			Thread.sleep(1000);	
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "ClickCheckIn");
			*/
			
			for(int i=0;i<7;i++)
				action.click(checkavailability.getDatePickerNext()).perform();
			Thread.sleep(1000);
			action.click(checkavailability.getDateItems().get(0)).perform();
			Thread.sleep(5000);	
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ChooseCheckIn");
			
			/*
			action.click(checkavailability.getCheckOut()).perform();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "ClickCheckOut");
			*/
			
			action.click(checkavailability.getDateItems().get(1)).perform();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "05-ChooseCheckOut");
			
			if(Browser.equals("IE")){
				action.click(scrollbuttons.getScrollDown()).perform();
				Thread.sleep(3000);
			}	
			
			action.click(checkavailability.getCheckAvailabilityButtonOnTop()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "06-ClickCheckAvailabilityButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageButtons(Map<?, ?> param) throws Exception {
		try {
			
			HomePage homepage = new HomePage(driver);
			ScrollButtons scrollbuttons = new ScrollButtons(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Actions action = new Actions(driver);
			
			action.click(scrollbuttons.getScrollDown()).perform();
			Thread.sleep(1000);
			homepage.getExploreParisDiscoverButton().click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickExploreParisDiscoverButton");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			homepage.getViewOfferButton().click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-ClickViewOfferButton");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			homepage.getHelloWeekendDiscoverButton().click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-ClickHelloWeekendDiscoverButton");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			homepage.getViewAllReviewsButton().click();
			Thread.sleep(15000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ClickViewAllReviewsButton");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());		
			Common.scrollTo((WebElement)homepage.getHalfReviews().toArray()[4]);
			Thread.sleep(1000);
			homepage.getLearnMoreButton().click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "05-ClickLearnMoreButton");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			homepage.getExploreNewYorkDiscoverButton().click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "06-ClickExploreNewYorkDiscoverButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HomePageFooter(Map<?, ?> param) throws Exception {
		try {
			
			Footer footer = new Footer(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Privacy privacy = new Privacy(driver);
			if(null != privacy.getPrivacyLink()&& null != privacy.getPrivacyLinkClose()){
				privacy.getPrivacyLinkClose().click();
				Thread.sleep(2000);
			}
			
			Actions action = new Actions(driver);
			
			action.click(footer.getLengsCollection()).perform();
			Thread.sleep(10000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "01-ClickLengsCollection");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			action.click(footer.getMillenniumCollection()).perform();
			Thread.sleep(10000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "02-ClickMillenniumCollection");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			action.click(footer.getCopthorneCollection()).perform();
			Thread.sleep(10000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "03-ClickCopthorneCollection");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			action.click(footer.getFacebook()).perform();
			Thread.sleep(20000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "04-ClickFacebook");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			action.click(footer.getTwitter()).perform();
			Thread.sleep(20000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "05-ClickTwitter");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			action.click(footer.getWeibo()).perform();
			Thread.sleep(20000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "06-ClickWeibo");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			Common.scrollTo(20000);
			action.click(footer.getSubscribeButton()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "07-ClickSubscribeButtonWithoutInput");
			
			action.sendKeys(footer.getNewsletterBox(), "Hi").perform();
			action.click(footer.getSubscribeButton()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "08-ClickSubscribeButtonWithInvalidEmail");
			
			for(int i=0;i<10;i++)
				action.sendKeys(footer.getNewsletterBox(), Keys.BACK_SPACE).perform();
			
			action.sendKeys(footer.getNewsletterBox(), "Rider.Wu@mullenloweprofero.com").perform();
			action.click(footer.getSubscribeButton()).perform();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "09-ClickSubscribeButtonWithValidEmail");
			
			for(int i = 0; i< footer.getFooterLinkItems().size(); i++)
			{
				Common.scrollTo(20000);
				Thread.sleep(1000);
				
				((WebElement)footer.getFooterLinkItems().toArray()[i]).click();
				Thread.sleep(20000);
				driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
				Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						 "10-ClickFooterLinkItems" + i);
				
				if(driver.getWindowHandles().size() <2){
					driver.navigate().back();
					Thread.sleep(20000);
				}
				else{
					driver.close();
					driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
					Thread.sleep(5000);
				}	
			}
			
			action.click(footer.getTermsOfUse()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "11-ClickTermsOfUse");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(footer.getSitemap()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "12-ClickSitemap");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(footer.getCookiePolicyAndDataSecurityNotice()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "13-ClickCookiePolicyAndDataSecurityNotice");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			action.click(footer.getSupplyChainTransparencyStatement()).perform();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					 "14-ClickSupplyChainTransparencyStatement");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
