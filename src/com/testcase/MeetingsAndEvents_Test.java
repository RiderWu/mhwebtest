package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.MeetingsAndEventsPage;
import com.pageobject.MeetingsAndEventsPage.MeetingsAndEventsResult;
import com.pageobject.MeetingsAndEventsPage.ResultWithoutFilter;
import com.web.Common;

//import com.utility.MailUtility;

public class MeetingsAndEvents_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MeetingsAndEventsPageLoad(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MeetingsAndEventsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MeetingsAndEventsFilters(Map<?, ?> param) throws Exception {
		try {

			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			MeetingsAndEventsPage meetingsandeventspage = new MeetingsAndEventsPage(driver);
			meetingsandeventspage.getScrollToExplore().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickScrollToExplore");
			
			meetingsandeventspage.getDestinations().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickDestinations");
			
			ScreenshotName = meetingsandeventspage.getDestinationsItems().get(0).getText();
			meetingsandeventspage.getDestinationsItems().get(0).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-"+ScreenshotName);
			
			ScreenshotName = meetingsandeventspage.getDestinationsItems().get(1).getText();
			meetingsandeventspage.getDestinationsItems().get(1).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-"+ScreenshotName);
			
			ScreenshotName = meetingsandeventspage.getDestinationsItems().get(1).getText();
			meetingsandeventspage.getDestinationsItems().get(1).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-"+ScreenshotName);
			
			meetingsandeventspage.getEventType().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickEventType");
			
			ScreenshotName = meetingsandeventspage.getEventTypeItems().get(0).getText();
			meetingsandeventspage.getEventTypeItems().get(0).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-"+ScreenshotName);
			
			meetingsandeventspage.getAttendees().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickAttendees");
			
			ScreenshotName = meetingsandeventspage.getAttendeesItems().get(0).getText();
			meetingsandeventspage.getAttendeesItems().get(0).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-"+ScreenshotName);
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"10-Filters");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MeetingsAndEventsLearnMore(Map<?, ?> param) throws Exception {
		try {

			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			MeetingsAndEventsPage meetingsandeventspage = new MeetingsAndEventsPage(driver);
			
			for(int i=0; i<4; i++){
				ResultWithoutFilter resultwithoutfilter=meetingsandeventspage.new ResultWithoutFilter(meetingsandeventspage.getResultsWithoutFilter().get(i));
				Common.scrollTo(resultwithoutfilter.getSelf());
				Thread.sleep(1000);
				ScreenshotName = resultwithoutfilter.getTitle().getText();
				resultwithoutfilter.getLearnMoreButton().click();
				Thread.sleep(10000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-"+ScreenshotName);
				
				driver.navigate().back();
				Thread.sleep(10000);
			}
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MeetingsAndEventsSocialShare(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			MeetingsAndEventsPage meetingsandeventspage = new MeetingsAndEventsPage(driver);
			
			meetingsandeventspage.getScrollToExplore().click();
			Thread.sleep(1000);
			
			meetingsandeventspage.getFacebook().click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Facebook");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			meetingsandeventspage.getTwitter().click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-Twitter");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			meetingsandeventspage.getWeibo().click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-Weibo");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MeetingsAndEventsResult(Map<?, ?> param) throws Exception {
		try {
			driver.navigate().to(Domain + "/" + Market);	
			Shared.SignIn(driver, "Rider.Wu@mullenloweprofero.com", "Profero@123");
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			MeetingsAndEventsPage meetingsandeventspage = new MeetingsAndEventsPage(driver);
			
			meetingsandeventspage.getScrollToExplore().click();
			Thread.sleep(1000);
			
			meetingsandeventspage.getAttendees().click();
			Thread.sleep(1000);
			meetingsandeventspage.getAttendeesItems().get(0).click();
			Thread.sleep(10000);
			
			MeetingsAndEventsResult meetingsandeventsresult = meetingsandeventspage.new MeetingsAndEventsResult(meetingsandeventspage.getMeetingsAndEventsResults().get(0));
			meetingsandeventsresult.getViewFeatures().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickViewFeatures");
			
			meetingsandeventsresult.getViewFeatures().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-UnClickViewFeatures");
			
			meetingsandeventsresult.getFavourite().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-AddFavourite");
			
			meetingsandeventsresult.getFavourite().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-RemoveFavourite");
			
			meetingsandeventsresult.getMoreInfoButton().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickMoreInfoButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_MeetingsAndEventsLoadMore(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			MeetingsAndEventsPage meetingsandeventspage = new MeetingsAndEventsPage(driver);
			
			meetingsandeventspage.getScrollToExplore().click();
			Thread.sleep(1000);
			
			meetingsandeventspage.getAttendees().click();
			Thread.sleep(1000);
			meetingsandeventspage.getAttendeesItems().get(0).click();
			Thread.sleep(10000);
			
			Common.scrollTo(0);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BeforeClickLoadMore");
			
			Common.scrollTo(meetingsandeventspage.getLoadMore());
			meetingsandeventspage.getLoadMore().click();
			Thread.sleep(1000);
			Common.scrollTo(0);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-AfterClickLoadMore");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
