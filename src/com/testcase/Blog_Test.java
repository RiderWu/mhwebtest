package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;

import com.pageobject.BlogPage;
import com.pageobject.BlogPage.BlogItem;

import com.web.Common;

//import com.utility.MailUtility;

public class Blog_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BlogPageLoad(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BlogPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BlogPageHeroImage(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			BlogPage blogpage = new BlogPage(driver);
			
			blogpage.getBreadcrumbLink().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickInspiration");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			blogpage.getLearnMore().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLearnMore");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BlogPageFilter(Map<?, ?> param) throws Exception {
		try {

			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			BlogPage blogpage = new BlogPage(driver);
			blogpage.getScrollToExplore().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickScrollToExplore");
			
			blogpage.getLocation().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickLocation");
			
			ScreenshotName = blogpage.getLocationItems().get(1).getText();
			blogpage.getLocationItems().get(1).click();
			Thread.sleep(10000);
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-"+ScreenshotName);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BlogPageExplore(Map<?, ?> param) throws Exception {
		try {

			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			BlogPage blogpage = new BlogPage(driver);
			
			BlogItem blogitem = blogpage.new BlogItem(blogpage.getBlogItems().get(0));
			Common.scrollTo(blogitem.getSelf());
			ScreenshotName = blogitem.getTitle().getText();
			blogitem.getExploreButton().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickExplore "+ScreenshotName);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BlogPageLoadMore(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			BlogPage blogpage = new BlogPage(driver);
			
			Common.scrollTo(blogpage.getLoadMore());
			blogpage.getLoadMore().click();
			Thread.sleep(5000);
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickLoadMore");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
