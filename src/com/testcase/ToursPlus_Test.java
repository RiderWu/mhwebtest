package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.ToursPlusPage;
import com.pageobject.ToursPlusPage.Tour;
import com.web.Common;

//import com.utility.MailUtility;

public class ToursPlus_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ToursPlusPageLoad(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ToursPlusPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ToursPlusSearch(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			ToursPlusPage tourspluspage = new ToursPlusPage(driver);
			tourspluspage.getSearchBox().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickSearchBox");
			
			tourspluspage.getSearchBox().sendKeys("Great Wall");
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-SearchSuggestionLoading");
			
			Thread.sleep(30000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-SearchSuggestionLoaded");
			
			tourspluspage.getSuggestions().get(tourspluspage.getSuggestions().size()-1).click();
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickSuggestion");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ToursPlusFilters(Map<?, ?> param) throws Exception {
		try {
			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			ToursPlusPage tourspluspage = new ToursPlusPage(driver);
			tourspluspage.getDestinationFilter().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickDestinationFilter");
			
			tourspluspage.getCities().get(0).click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickBeijingLoading");
			
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickBeijingLoaded");
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			
			tourspluspage.getCategoryFilter().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickCategoryFilter");
			
			tourspluspage.getCategoryItems().get(0).click();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickCategoryItemLoading");
			
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickCategoryItemLoaded");
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			
			tourspluspage.getSelectButton().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickSelectButton");
			
			ScreenshotName=tourspluspage.getOrderItems().get(3).getText();
			tourspluspage.getOrderItems().get(3).click();
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-"+ScreenshotName);
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			
			tourspluspage.getClearAll().click();
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ClickClearAll");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ToursPlusTop10Destinations(Map<?, ?> param) throws Exception {
		try {
			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			ToursPlusPage tourspluspage = new ToursPlusPage(driver);
			
			for(int i=0;i<tourspluspage.getTop10Destinations().size();i++){
				ScreenshotName=tourspluspage.getTop10Destinations().get(i).getText();
				tourspluspage.getTop10Destinations().get(i).click();
				Thread.sleep(10000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-"+ScreenshotName);
				
				Common.scrollTo(0);
				Thread.sleep(1000);
				
				tourspluspage.getClearAll().click();
				Thread.sleep(1000);
			}
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ToursPlusHotelsDetails(Map<?, ?> param) throws Exception {
		try {
			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			ToursPlusPage tourspluspage = new ToursPlusPage(driver);
			
			for(int i=0;i<tourspluspage.getHotelDetailsButtons().size();i++){
				Common.scrollTo(tourspluspage.getPopularHotels().get(i).getLocation().getY()-200);
				ScreenshotName=tourspluspage.getPopularHotels().get(i).getText();
				tourspluspage.getHotelDetailsButtons().get(i).click();
				Thread.sleep(10000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-"+ScreenshotName);
				
				driver.navigate().back();
				Thread.sleep(10000);
			}
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ToursPlusTour(Map<?, ?> param) throws Exception {
		try {
			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market);	
			Shared.SignIn(driver, "Rider.Wu@mullenloweprofero.com", "Profero@123");
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			ToursPlusPage tourspluspage = new ToursPlusPage(driver);
			
			Tour tour = tourspluspage.new Tour(tourspluspage.getTours().get(0));
			ScreenshotName = tour.getTitle().getText();
			
			Common.scrollTo(tour.getSelf().getLocation().getY()-100);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-"+ScreenshotName);
			
			tour.getFavorite().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-AddFavorite");
			
			tour.getFavorite().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-RemoveFavorite");
			
			tour.getSocialShare().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickSocialShare");
			
			tour.getBookNowButton().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickBookNowButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_ToursPlusLoadMoreTours(Map<?, ?> param) throws Exception {
		try {
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BeforeClickLoadMoreTours");
			
			ToursPlusPage tourspluspage = new ToursPlusPage(driver);
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			tourspluspage.getDestinationFilter().click();
			Thread.sleep(1000);
			tourspluspage.getCities().get(0).click();
			Thread.sleep(1000);
			Common.scrollTo(tourspluspage.getLoadMoreTours().getLocation().getY()-200);
			Thread.sleep(1000);
			tourspluspage.getLoadMoreTours().click();
			Thread.sleep(1000);
			Common.scrollTo(0);
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-AfterClickLoadMoreTours");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
