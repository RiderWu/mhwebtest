package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.web.Common;
import com.pageobject.BookingPage;
import com.pageobject.BookingPage.RoomTypeItem;
import com.pageobject.BookingPage.RoomTypeItem.RateItem;
import com.pageobject.Booking_RoommodifyPage;
import com.pageobject.MyMillennium_Checkout_ConfirmationPage;
import com.pageobject.MyMillennium_Checkout_GuestsInfoPage;
import com.pageobject.OfferDetailsPage;
import com.pageobject.ViewOrModifyYourBookingPage;

public class Booking_Test {

	public static String startTime = null;
	public WebDriver driver = null;
	public static String domain = null;
	public static String browser = null;
	public static String market = null;
	public static String width = null;
	public static String height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(@Optional("https://www.millenniumhotels.com") String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		startTime = datetime.format(new Date());
		domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(@Optional("Chrome") String _browser, @Optional("en") String _market, @Optional("1366") String _width, @Optional("768") String _height) throws Exception {
		browser = _browser;
		market = _market;
		width = _width;
		height = _height;
		driver = Common.openBrowser(browser, "about:blank", _width, _height);
		
		driver.navigate().to(domain + "/" + market);	
		Shared.SignIn(driver, "Rider.Wu@mullenloweprofero.com", "Profero@123");
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BookingWithCreditCard(Map<?, ?> param) throws Exception {
		try {
			BookingPage bookingpage = new BookingPage(driver);
			
			driver.navigate().to(domain + "/" + market + (String) param.get("URL"));
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-BookingPageLoad");
			
			
			//Shared.SearchAvailableHotel(driver, "Mayfair");
			
			RoomTypeItem roomtypeitem = bookingpage.new RoomTypeItem((WebElement)bookingpage.getRoomTypeItems().toArray()[0]);
			roomtypeitem.getSelectButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-BookingRateLoading");
			Thread.sleep(15000);
			
			Common.scrollTo(roomtypeitem.getSelectRateTitle());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-BookingRateLoaded");
			
			roomtypeitem.getViewAllRates().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ViewAllRates");
			
			RateItem rateitem = roomtypeitem.new RateItem((WebElement)roomtypeitem.getRateItems().toArray()[1]);
			rateitem.getRadioButton().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-SelectTheFlexibleRate");
			
			bookingpage.getCheckoutButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-CheckoutLoading");
			
			//bookingpage.getCheckoutButton().click();
			int count = 0;
			while(driver.getCurrentUrl().contains("booking") && count < 30)
			{
				Thread.sleep(1000);
				count++;
			}
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-CheckoutInformation");
			
			MyMillennium_Checkout_GuestsInfoPage mcgpage = new MyMillennium_Checkout_GuestsInfoPage(driver);
			
			mcgpage.getPayWithCreditCard().click();
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickPayWithCreditCard");
			
			mcgpage.getCreditCardholdersName().sendKeys("Tester");
			mcgpage.getCreditCardNumber().sendKeys("4567350000427977");
			mcgpage.getCreditCardSecurityCode().sendKeys("112");
			Select month = new Select(mcgpage.getCreditCardExpiryMonth());
			month.selectByValue("11");
			Select year = new Select(mcgpage.getCreditCardExpiryYear());
			year.selectByValue("2017");
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-FillCreditCardInfo");
			
			mcgpage.getCreditCardArrow().click();
			Thread.sleep(1000);
			mcgpage.getPayWithCreditCard().click();
			Thread.sleep(1000);
			
			mcgpage.getBookNowButton().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"10-ClickBookNowButton");
			Thread.sleep(30000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"11-Confirmation");
			
			MyMillennium_Checkout_ConfirmationPage mccpage = new MyMillennium_Checkout_ConfirmationPage(driver);
			
			String LastName = mccpage.getCustomerName().getText().split(" ")[mccpage.getCustomerName().getText().split(" ").length-1];
			String ConfirmationNumber = mccpage.getConfirmationNumber().getText();
			
			driver.navigate().to(domain + "/" + market + "/view-or-modify-your-booking");
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"12-ViewOrModifyYourBooking");
			
			ViewOrModifyYourBookingPage vomybpage = new ViewOrModifyYourBookingPage(driver);
			vomybpage.getLastName().sendKeys(LastName);
			vomybpage.getConfirmationNumber().sendKeys(ConfirmationNumber);
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"13-FillLastNameAndConfirmationNumber");
			
			vomybpage.getViewBooking().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"14-ClickViewBookingButton");
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			
			Booking_RoommodifyPage brPage = new Booking_RoommodifyPage(driver);
			brPage.getEditButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"15-ClickEditButton");
			
			brPage.getSaveButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"16-ClickSaveButtonLoading");
			
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"17-ClickSaveButtonLoaded");
			
			brPage.getNextStepButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"18-ClickNextStepButtonLoading");
			
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"19-ClickNextStepButtonLoaded");
			
			mcgpage.getBookNowButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"20-ClickBookNowButtonLoading");
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"21-ClickBookNowButtonLoaded");
			
			driver.navigate().to(domain + "/" + market + "/view-or-modify-your-booking");
			vomybpage = new ViewOrModifyYourBookingPage(driver);
			vomybpage.getLastName().sendKeys(LastName);
			vomybpage.getConfirmationNumber().sendKeys(ConfirmationNumber);
			Thread.sleep(1000);
			vomybpage.getViewBooking().click();
			Thread.sleep(15000);
			brPage.getCancelButton().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"22-ClickCancelButton");
			
			brPage.getCancelBookingButton().click();
			Thread.sleep(30000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"23-ClickCancelBookingButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BookingWithNonCreditCard(Map<?, ?> param) throws Exception {
		try {
			BookingPage bookingpage = new BookingPage(driver);
			
			driver.navigate().to(domain + "/" + market + (String) param.get("URL"));
			Thread.sleep(10000);
			
			RoomTypeItem roomtypeitem = bookingpage.new RoomTypeItem((WebElement)bookingpage.getRoomTypeItems().toArray()[0]);
			roomtypeitem.getSelectButton().click();
			Thread.sleep(20000);
			
			Common.scrollTo(roomtypeitem.getSelectRateTitle());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-SelectTheLowestRate");
			
			bookingpage.getCheckoutButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-CheckoutLoading");
			
			int count = 0;
			while(driver.getCurrentUrl().contains("booking") && count < 30)
			{
				Thread.sleep(1000);
				count++;
			}
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-CheckoutInformation");
			
			Common.scrollTo(2000);
			Thread.sleep(1000);
			
			MyMillennium_Checkout_GuestsInfoPage mcgpage = new MyMillennium_Checkout_GuestsInfoPage(driver);
			
			mcgpage.getOtherPaymentMethods().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickOtherPaymentMethods");
			
			Common.scrollTo(mcgpage.getOtherPaymentMethods());
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ScrollToOtherPaymentMethods");
			
			mcgpage.getPayWithChinaUnionPay().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickPayWithChinaUnionPayLoading");
			
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickPayWithChinaUnionPayLoaded");
			
			mcgpage.getPayWithAlipay().click();
			Thread.sleep(3000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickPayWithAlipayLoading");
			
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ClickPayWithAlipayLoaded");
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			
			mcgpage.getRemoveLink().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"10-ClickRemoveLink");
			
			mcgpage.getRemoveButton().click();
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"11-ClickRemoveButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_BookOffer(Map<?, ?> param) throws Exception {
		try {
			OfferDetailsPage offerdetailspage = new OfferDetailsPage(driver);
			
			driver.navigate().to(domain + "/" + market + (String) param.get("URL"));
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-"+offerdetailspage.getOfferTitle().getText());
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			
			offerdetailspage.getCheckAvailabilityButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickCheckAvailabilityButtonLoading");
			
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-ClickCheckAvailabilityButtonLoaded");
			
			Common.scrollTo((WebElement)offerdetailspage.getRoomItems().toArray()[0]);
			Thread.sleep(1000);
			
			offerdetailspage.getCheckoutButton().click();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickCheckoutButtonLoading");
			
			Thread.sleep(20000);
			Common.ScrollDownAndTakeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-ClickCheckoutButtonLoaded");
			
			MyMillennium_Checkout_GuestsInfoPage mcgpage = new MyMillennium_Checkout_GuestsInfoPage(driver);
			
			Common.scrollTo(0);
			Thread.sleep(1000);
			
			mcgpage.getRemoveLink().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickRemoveLink");
			
			mcgpage.getRemoveButton().click();
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-ClickRemoveButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, browser+"_"+market+"_"+width+"_"+height, startTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
