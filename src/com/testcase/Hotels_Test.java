package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.web.Common;
import com.pageobject.HotelsPage;

public class Hotels_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HotelsPageLoad(Map<?, ?> param) throws Exception {
		try {
		
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-HotelsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HotelsPageTop10(Map<?, ?> param) throws Exception {
		try {
		
			HotelsPage hotelspage = new HotelsPage(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			for(int i = 0; i<hotelspage.getTop10Hotels().size(); i++) {
				String hotel = ((WebElement)hotelspage.getTop10Hotels().toArray()[i]).getText();
				((WebElement)hotelspage.getTop10Hotels().toArray()[i]).click();
				Thread.sleep(15000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"0" + (i+1) + "-" + hotel);
				
				driver.navigate().back();
				Thread.sleep(10000);
			}
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HotelsPageCities(Map<?, ?> param) throws Exception {
		try {
		
			HotelsPage hotelspage = new HotelsPage(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.scrollTo(hotelspage.getBeijing());
			Thread.sleep(1000);
			hotelspage.getBeijing().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Beijing");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Common.scrollTo(hotelspage.getChengdu());
			Thread.sleep(1000);
			hotelspage.getChengdu().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-Chengdu");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Common.scrollTo(hotelspage.getSingapore());
			Thread.sleep(1000);
			hotelspage.getSingapore().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-Singapore");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Common.scrollTo(hotelspage.getLondon());
			Thread.sleep(1000);
			hotelspage.getLondon().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-London");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Common.scrollTo(hotelspage.getNewYork());
			Thread.sleep(1000);
			hotelspage.getNewYork().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-NewYork");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HotelsPageHotels(Map<?, ?> param) throws Exception {
		try {
		
			HotelsPage hotelspage = new HotelsPage(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.scrollTo(hotelspage.getMillenniumResidencesBeijingFortunePlaza());
			Thread.sleep(1000);
			hotelspage.getMillenniumResidencesBeijingFortunePlaza().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-MillenniumResidencesBeijingFortunePlaza");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Common.scrollTo(hotelspage.getMHotelChengdu());
			Thread.sleep(1000);
			hotelspage.getMHotelChengdu().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-MHotelChengdu");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Common.scrollTo(hotelspage.getCopthorneKingsHotel());
			Thread.sleep(1000);
			hotelspage.getCopthorneKingsHotel().click();
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-CopthorneKingsHotel");
			
			driver.navigate().back();
			Thread.sleep(10000);
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_HotelsPageViewDestinationMap(Map<?, ?> param) throws Exception {
		try {
		
			HotelsPage hotelspage = new HotelsPage(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.scrollTo(0);
			hotelspage.getViewDestinationMap().click();
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ViewDestinationMap");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
