package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.OffersPage;
import com.pageobject.OffersPage.Offer;
import com.pageobject.Privacy;
import com.pageobject.SignIn;
import com.web.Common;

//import com.utility.MailUtility;

public class Offers_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OffersPageLoad(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-OffersPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OffersPageFilters(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			OffersPage offerspage = new OffersPage(driver);
			String ScreenshotName = null;
			
			SignIn signin = new SignIn(driver);
			signin.getAvatar().click();
			
			Common.scrollTo(offerspage.getDestinationFilter());
			Thread.sleep(1000);
			offerspage.getDestinationFilter().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickDestinationFilter");
			
			ScreenshotName=((WebElement)offerspage.getCountries().toArray()[0]).getText();
			((WebElement)offerspage.getCountries().toArray()[0]).click();
			Common.scrollTo(0);
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-"+ScreenshotName);
			
			Common.scrollTo(offerspage.getDestinationFilter());
			offerspage.getDestinationFilter().click();
			Thread.sleep(1000);
			
			ScreenshotName=((WebElement)offerspage.getCities().toArray()[0]).getText();
			((WebElement)offerspage.getCities().toArray()[0]).click();
			Common.scrollTo(0);
			Thread.sleep(15000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-"+ScreenshotName);
			
			Common.scrollTo(offerspage.getHotelFilter());
			offerspage.getHotelFilter().click();
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-ClickHotelFilter");
			
			ScreenshotName=((WebElement)offerspage.getHotels().toArray()[1]).getText();
			((WebElement)offerspage.getHotels().toArray()[1]).click();
			Common.scrollTo(0);
			Thread.sleep(5000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-"+ScreenshotName);
			
			Common.scrollTo(0);
			offerspage.getClearAll().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickClearAll");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OffersPageTop10Destinations(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			OffersPage offerspage = new OffersPage(driver);
			String ScreenshotName = null;
			
			SignIn signin = new SignIn(driver);
			signin.getAvatar().click();
			
			for(int i=0; i<offerspage.getTop10Destinations().size();i++) {
				ScreenshotName=((WebElement)offerspage.getTop10Destinations().toArray()[i]).getText();
				((WebElement)offerspage.getTop10Destinations().toArray()[i]).click();
				Common.scrollTo(0);
				Thread.sleep(20000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-"+ScreenshotName);
			}
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OffersPagePopularHotels(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Privacy privacy = new Privacy(driver);
			if(null != privacy.getPrivacyLink()&& null != privacy.getPrivacyLinkClose()){
				privacy.getPrivacyLinkClose().click();
				Thread.sleep(2000);
			}
			
			OffersPage offerspage = new OffersPage(driver);
			String ScreenshotName = null;
			
			SignIn signin = new SignIn(driver);
			signin.getAvatar().click();
			
			for(int i=0; i<offerspage.getPopularHotels().size();i++) {
				ScreenshotName=((WebElement)offerspage.getPopularHotels().toArray()[i]).getText();
				((WebElement)offerspage.getHotelDetailsButtons().toArray()[i]).click();
				Common.scrollTo(0);
				Thread.sleep(20000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-"+ScreenshotName);
				
				driver.navigate().back();
				Thread.sleep(10000);
			}
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_OffersPageOffers(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market);	
			Shared.SignIn(driver, "Rider.Wu@mullenloweprofero.com", "Profero@123");
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			OffersPage offerspage = new OffersPage(driver);
			String ScreenshotName = null;
			
			//SignIn signin = new SignIn(driver);
			//signin.getAvatar().click();
			
			Offer offer = offerspage.new Offer((WebElement)offerspage.getOffers().toArray()[0]);
			
			Common.scrollTo((WebElement)offerspage.getOffers().toArray()[0]);
			Thread.sleep(1000);
			ScreenshotName=offer.getTitle().getText();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-"+ScreenshotName);
			
			offer.getBookNow().click();
			Thread.sleep(20000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-"+ScreenshotName+"-BookNow");
			
			driver.navigate().back();
			Thread.sleep(10000);
			offer = offerspage.new Offer((WebElement)offerspage.getOffers().toArray()[0]);
			
			offer.getFavourite().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-"+ScreenshotName+"-AddFavourite");
			
			offer.getFavourite().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-"+ScreenshotName+"-RemoveFavourite");
			
			Thread.sleep(5000);
			
			offer.getSocialShare().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-"+ScreenshotName+"-ClickSocialShare");
			
			Common.scrollTo(offerspage.getLoadMoreOffers());
			offerspage.getLoadMoreOffers().click();
			Thread.sleep(10000);
			Common.scrollTo(0);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickLoadMoreOffers");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
