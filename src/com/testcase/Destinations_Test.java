package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.DestinationsPage;
import com.pageobject.Privacy;

import com.web.Common;

import com.testcase.Shared;

public class Destinations_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_DestinationsPageLoad(Map<?, ?> param) throws Exception {
		try {
		
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-DestinationsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_DestinationsPageMap(Map<?, ?> param) throws Exception {
		try {
			String Screenshotname = "";
		
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			Thread.sleep(5000);
			
			Shared.SignIn(driver, "Rider.Wu@mullenloweprofero.com", "Profero@123");
			
			Privacy privacy = new Privacy(driver);
			if(null != privacy.getPrivacyLink() && null != privacy.getPrivacyLinkClose()){
				privacy.getPrivacyLinkClose().click();
				Thread.sleep(2000);
			}
			
			DestinationsPage destinationspage = new DestinationsPage(driver);
			
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-Global");
			
			Screenshotname = ((WebElement)destinationspage.getRegions().toArray()[1]).getText();
			
			((WebElement)destinationspage.getRegions().toArray()[1]).click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-"+Screenshotname);
			
			Screenshotname = ((WebElement)destinationspage.getCountries().toArray()[5]).getText();
			((WebElement)destinationspage.getCountries().toArray()[5]).click();
			Thread.sleep(20000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-"+Screenshotname);
			
			Screenshotname = ((WebElement)destinationspage.getHotels().toArray()[0]).getText();
			((WebElement)destinationspage.getHotels().toArray()[0]).click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-"+Screenshotname);
			
			Screenshotname = ((WebElement)destinationspage.getHotels().toArray()[0]).getText();
			((WebElement)destinationspage.getHotels().toArray()[0]).click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-"+Screenshotname);
			
			Screenshotname = ((WebElement)destinationspage.getHotels().toArray()[0]).getText();
			((WebElement)destinationspage.getHotels().toArray()[0]).click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-"+Screenshotname);
			
			Screenshotname = ((WebElement)destinationspage.getHotels().toArray()[1]).getText();
			((WebElement)destinationspage.getHotels().toArray()[1]).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-BackTo"+Screenshotname);
			
			destinationspage.getViewDestinationList().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-ClickViewDestinationList");
			
			destinationspage.getViewDestinationMap().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"09-ClickViewDestinationMap");
			
			destinationspage.getHotelHeroImage().click();
			Thread.sleep(10000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"10-ClickHotelHeroImage");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			destinationspage.getHotelNameTitle().click();
			Thread.sleep(5000);
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"11-ClickHotelNameTitle");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			destinationspage.getFavourite().click();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"12-ClickAddFavouriteWithSignIn");
			destinationspage.getFavourite().click();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"13-ClickRemoveFavouriteWithSignIn");
			
			destinationspage.getSeeReviews().click();
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"14-ClickSeeReviews");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			destinationspage.getHotelDetails().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"15-ClickHotelDetails");
			
			driver.navigate().back();
			Common.scrollTo(0);
			FindDestinationHotel(destinationspage);
			
			destinationspage.getBookNow().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"16-ClickBookNow");
			
			driver.navigate().back();
			FindDestinationHotel(destinationspage);
			
			destinationspage.getNearbyAttractions().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"17-ClickNearbyAttractions");
			
			Screenshotname = ((WebElement)destinationspage.getAttractionsList().toArray()[1]).getText();
			((WebElement)destinationspage.getAttractionsList().toArray()[1]).click();
			Thread.sleep(10000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"18-"+Screenshotname);
			
			destinationspage.getViewNearestHotel().click();
			Thread.sleep(2000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"19-ClickViewNearestHotel");
			
			destinationspage.getNearbyTours().click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"20-ClickNearbyTours");
			
			Screenshotname = ((WebElement)destinationspage.getToursList().toArray()[0]).getText();
			((WebElement)destinationspage.getToursList().toArray()[0]).click();
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"21-"+Screenshotname);
			
			destinationspage.getTourFavourite().click();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"22-ClickAddFavouriteWithSignIn");
			destinationspage.getTourFavourite().click();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"23-ClickRemoveFavouriteWithSignIn");
			
			destinationspage.getSeeTourReviews().click();
			driver.switchTo().window(driver.getWindowHandles().toArray()[driver.getWindowHandles().size()-1].toString());
			Thread.sleep(15000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"24-ClickSeeReviews");
			
			driver.close();
			driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
			
			destinationspage.getMoreDetails().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"25-ClickMoreDetails");
			
			destinationspage.getBookTourNow().click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"26-ClickBookNow");
			
			driver.navigate().back();
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	public static void FindDestinationHotel(DestinationsPage destinationspage) throws Exception {
		((WebElement)destinationspage.getRegions().toArray()[1]).click();
		Thread.sleep(1000);
		((WebElement)destinationspage.getCountries().toArray()[5]).click();
		Thread.sleep(1000);
		((WebElement)destinationspage.getHotels().toArray()[1]).click();
		Thread.sleep(15000);
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_DestinationsPageTop10(Map<?, ?> param) throws Exception {
		try {
		
			DestinationsPage destinationspage = new DestinationsPage(driver);
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			for(int i = 0; i<destinationspage.getTop10Destinations().size(); i++) {
				String city = ((WebElement)destinationspage.getTop10Destinations().toArray()[i]).getText();
				((WebElement)destinationspage.getTop10Destinations().toArray()[i]).click();
				Thread.sleep(15000);
				Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"0" + (i+1) + "-" + city);
				
				driver.navigate().back();
				Thread.sleep(10000);
			}
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
