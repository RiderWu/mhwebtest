package com.testcase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.dataprovider.TestDataProvider;
import com.pageobject.WeddingsPage;
import com.pageobject.WeddingsPage.Wedding;
import com.pageobject.WeddingsPage.WeddingStory;
import com.web.Common;

//import com.utility.MailUtility;

public class Weddings_Test {

	public static String StartTime = null;
	public WebDriver driver = null;
	public static String Domain = null;
	public static String Browser = null;
	public static String Market = null;
	public static String Width = null;
	public static String Height = null;

	@Parameters({ "environment" })
	@BeforeSuite
	public void beforeSuite(String environment) throws Exception {
		SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		StartTime = datetime.format(new Date());
		Domain = environment;
	}

	@AfterSuite
	public void afterSuite() throws Exception {
		// MailUtility.sendMail();
	}

	@Parameters({ "browser", "market", "width", "height" })
	@BeforeClass
	public void beforeClass(String browser, String market, String width, String height) throws Exception {
		Browser = browser;
		Market = market;
		Width = width;
		Height = height;
		driver = Common.openBrowser(Browser, "about:blank", width, height);
		
	}

	@AfterClass
	public void afterClass() throws Exception {
		driver.quit();
		
	}

	@BeforeMethod
	public void beforeMethod() throws Exception {

	}

	@AfterMethod
	public void afterMethod() throws Exception {
		
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_WeddingsPageLoad(Map<?, ?> param) throws Exception {
		try {

			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-WeddingsPageLoad");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_WeddingsFilters(Map<?, ?> param) throws Exception {
		try {

			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			WeddingsPage weddingspage = new WeddingsPage(driver);
			weddingspage.getScrollToExplore().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-ClickScrollToExplore");
			
			weddingspage.getDestinations().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickDestinations");
			
			ScreenshotName = weddingspage.getDestinationsItems().get(0).getText();
			weddingspage.getDestinationsItems().get(0).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"03-"+ScreenshotName);
			
			ScreenshotName = weddingspage.getDestinationsItems().get(2).getText();
			weddingspage.getDestinationsItems().get(2).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"04-"+ScreenshotName);
			
			ScreenshotName = weddingspage.getDestinationsItems().get(1).getText();
			weddingspage.getDestinationsItems().get(1).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"05-"+ScreenshotName);
			
			weddingspage.getFilterBy().click();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"06-ClickFilterBy");
			
			ScreenshotName = weddingspage.getInterests().get(0).getText();
			weddingspage.getInterests().get(0).click();
			Thread.sleep(5000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"07-"+ScreenshotName);
			
			weddingspage.getFilterBy().click();
			Thread.sleep(1000);
			Common.scrollTo(0);
			Thread.sleep(1000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"08-Filters");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_WeddingsFindOutMore(Map<?, ?> param) throws Exception {
		try {

			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			WeddingsPage weddingspage = new WeddingsPage(driver);
			
			Wedding wedding = weddingspage.new Wedding(weddingspage.getWedding().get(0));
			Common.scrollTo(wedding.getSelf());
			ScreenshotName = wedding.getTitle().getText();
			Thread.sleep(1000);
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"01-"+ScreenshotName);
			
			wedding.getFindOutMoreButton().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickFindOutMoreButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
	
	@Test(dataProvider = "dataProvider", dataProviderClass = TestDataProvider.class)
	public void Test_WeddingsStories(Map<?, ?> param) throws Exception {
		try {

			String ScreenshotName = null;
			
			driver.navigate().to(Domain + "/" + Market + (String) param.get("URL"));
			
			WeddingsPage weddingspage = new WeddingsPage(driver);
			
			WeddingStory weddingstory = weddingspage.new WeddingStory(weddingspage.getWeddingStory());
			Common.scrollTo(weddingstory.getSelf());
			Thread.sleep(1000);
			
			for(int i=0; i<5; i++){
				ScreenshotName = weddingstory.getTitle().getText();
				Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
						"01-"+ScreenshotName);
				weddingspage.getNextButton().click();
				Thread.sleep(5000);
			}
			
			weddingstory.getReadTheirStoryButton().click();
			Thread.sleep(10000);
			Common.ScrollDownAndTakeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"02-ClickReadTheirStoryButton");
			
			Assert.assertTrue(driver.getPageSource().length() > 11000, "Page content is too short, maybe Error now!");

		} catch (Exception e) {

			e.printStackTrace();
			Common.takeScreenShot(driver, null, Browser+"_"+Market+"_"+Width+"_"+Height, StartTime, Thread.currentThread().getStackTrace()[1].getMethodName(), 
					"99-ERROR");
			Assert.assertTrue(false, e.getMessage());
		}
	}
}
