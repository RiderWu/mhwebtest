package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class ToursPlusPage {
public static WebDriver driver = null;
	
	public ToursPlusPage(WebDriver driver){
		ToursPlusPage.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(tagName = "input")
	private WebElement SearchBox;
	public WebElement getSearchBox()
	{
		return SearchBox;
	}
	
	@FindBys({
		@FindBy(className = "search-result"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Suggestions;
	public List<WebElement> getSuggestions()
	{
		return Suggestions;
	}
	
	@FindBy(className = "destination-filter")
	private WebElement DestinationFilter;
	public WebElement getDestinationFilter()
	{
		return DestinationFilter;
	}
	
	@FindBys({
		@FindBy(className = "destination-list"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Cities;
	public List<WebElement> getCities()
	{
		return Cities;
	}
	
	@FindBy(xpath = "//div[3]/div[2]/div")
	private WebElement CategoryFilter;
	public WebElement getCategoryFilter()
	{
		return CategoryFilter;
	}
	
	@FindBy(xpath = "//div[2]/div[2]/ul/li")
	private List<WebElement> CategoryItems;
	public List<WebElement> getCategoryItems()
	{
		return CategoryItems;
	}
	
	@FindBy(className = "select-button")
	private WebElement SelectButton;
	public WebElement getSelectButton()
	{
		return SelectButton;
	}
	
	@FindBy(xpath = "//div[3]/div[2]/ul/li")
	private List<WebElement> OrderItems;
	public List<WebElement> getOrderItems()
	{
		return OrderItems;
	}
	
	@FindBy(css = "a.clear-all.item")
	private WebElement ClearAll;
	public WebElement getClearAll()
	{
		return ClearAll;
	}
	
	@FindBys({
		@FindBy(className = "sidebar-destination"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Top10Destinations;
	public List<WebElement> getTop10Destinations()
	{
		return Top10Destinations;
	}
	
	@FindBys({
		@FindBy(className = "sidebar-hotel"),
		@FindBy(tagName = "h6")
	})
	private List<WebElement> PopularHotels;
	public List<WebElement> getPopularHotels()
	{
		return PopularHotels;
	}
	
	@FindBys({
		@FindBy(className = "sidebar-hotel"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> HotelDetailsButtons;
	public List<WebElement> getHotelDetailsButtons()
	{
		return HotelDetailsButtons;
	}
	
	@FindBy(className = "tpl-static-persuade")
	private WebElement StaticPersuade;
	public WebElement getStaticPersuade()
	{
		return StaticPersuade;
	}
	
	@FindBy(className = "load-more")
	private WebElement LoadMoreTours;
	public WebElement getLoadMoreTours()
	{
		return LoadMoreTours;
	}
	
	@FindBy(className = "always-show-image")
	private List<WebElement> Tours;
	public List<WebElement> getTours()
	{
		return Tours;
	}
	
	public class Tour {
		
		private WebElement Tour;
		
		public Tour(WebElement Tour) {
			this.Tour = Tour;
		}
		
		public WebElement getSelf()
		{
			return Tour;
		}
		
		public WebElement getTitle()
		{
			return Tour.findElement(By.className("tpl-text")).findElement(By.xpath("h3/span[1]"));
		}
		
		public WebElement getFavorite()
		{
			return Tour.findElement(By.className("tpl-text")).findElement(By.xpath("h3/a"));
		}
		
		public WebElement getSocialShare()
		{
			return Tour.findElement(By.className("tpl-text")).findElement(By.xpath("h3/span[2]"));
		}
		
		public WebElement getBookNowButton()
		{
			return Tour.findElement(By.className("wgt-sqbutton"));
		}
	}
	
	
}
