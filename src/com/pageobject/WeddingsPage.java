package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class WeddingsPage {
public static WebDriver driver = null;
	
	public WeddingsPage(WebDriver driver){
		WeddingsPage.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "wgt-scrolldown")
	private WebElement ScrollToExplore;
	public WebElement getScrollToExplore()
	{
		return ScrollToExplore;
	}
	
	@FindBy(xpath = "//fieldset/div")
	private WebElement Destinations;
	public WebElement getDestinations()
	{
		return Destinations;
	}
	
	@FindBy(xpath = "//fieldset/ul/li")
	private List<WebElement> DestinationsItems;
	public List<WebElement> getDestinationsItems()
	{
		return DestinationsItems;
	}
	
	@FindBy(xpath = "//div[2]/fieldset/div")
	private WebElement FilterBy;
	public WebElement getFilterBy()
	{
		return FilterBy;
	}
	
	@FindBy(xpath = "//div[2]/fieldset/ul/li/ul/li")
	private List<WebElement> Interests;
	public List<WebElement> getInterests()
	{
		return Interests;
	}
	
	@FindBy(xpath = "//div[4]/div/div")
	private List<WebElement> Wedding;
	public List<WebElement> getWedding()
	{
		return Wedding;
	}
	
	public class Wedding{
		private WebElement Wedding;
		
		public Wedding(WebElement Wedding) {
			this.Wedding = Wedding;
		}
		
		public WebElement getSelf()
		{
			return Wedding;
		}
		
		public WebElement getTitle()
		{
			return Wedding.findElement(By.xpath("div/div/h3"));
		}
		
		public WebElement getFindOutMoreButton()
		{
			return Wedding.findElement(By.className("wgt-sqbutton"));
		}
	}
	
	@FindBy(xpath = "//div[2]/div/div[2]/div")
	private WebElement WeddingStory;
	public WebElement getWeddingStory()
	{
		return WeddingStory;
	}
	
	public class WeddingStory{
		private WebElement WeddingStory;
		
		public WeddingStory(WebElement WeddingStory) {
			this.WeddingStory = WeddingStory;
		}
		
		public WebElement getSelf()
		{
			return WeddingStory;
		}
		
		public WebElement getTitle()
		{
			return WeddingStory.findElement(By.className("tpl-text")).findElement(By.tagName("h3"));
		}
		
		public WebElement getReadTheirStoryButton()
		{
			return WeddingStory.findElement(By.className("wgt-sqbutton"));
		}
	}
	
	@FindBy(className = "wgt-gallerybtn-previous")
	private WebElement PreviousButton;
	public WebElement getPreviousButton()
	{
		return PreviousButton;
	}
	
	@FindBy(className = "wgt-gallerybtn-next")
	private WebElement NextButton;
	public WebElement getNextButton()
	{
		return NextButton;
	}
}
