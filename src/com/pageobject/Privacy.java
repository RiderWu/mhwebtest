package com.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class Privacy {
public WebDriver driver = null;
	
	public Privacy(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getPrivacyLink()
	{
		try{
			return driver.findElement(By.className("global-cookie"));
		}
		catch(NoSuchElementException ex){
			return null;
		}
	}
	
	public WebElement getPrivacyLinkClose()
	{
		try{
			return driver.findElement(By.id("cookie-policy-close"));
		}
		catch(NoSuchElementException ex){
			return null;
		}
	}
}
