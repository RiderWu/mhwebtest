package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class OfferDetailsPage {
public static WebDriver driver = null;
	
	public OfferDetailsPage(WebDriver driver){
		OfferDetailsPage.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBys({
		@FindBy(className = "tpl-text"),
		@FindBy(tagName = "p")
	})
	private WebElement OfferTitle;
	public WebElement getOfferTitle()
	{
		return OfferTitle;
	}
	
	@FindBy(className = "check-availability")
	private WebElement CheckAvailabilityButton;
	public WebElement getCheckAvailabilityButton()
	{
		return CheckAvailabilityButton;
	}
	
	@FindBy(className = "room-module-item")
	private List<WebElement> RoomItems;
	public List<WebElement> getRoomItems()
	{
		return RoomItems;
	}
	
	@FindBy(xpath = "//div/div/div[4]/div/a")
	private WebElement CheckoutButton;
	public WebElement getCheckoutButton()
	{
		return CheckoutButton;
	}
}
