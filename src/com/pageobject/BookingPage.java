package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class BookingPage {
	
	public WebDriver driver = null;
	
	public BookingPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public List<WebElement> getRoomTypeItems() {
		return driver.findElements(By.className("gbw-p-s-room-list-item"));
	}
	
	public class RoomTypeItem {
		
		private WebElement RoomTypeItem;
		
		public RoomTypeItem(WebElement RoomTypeItem) {
			this.RoomTypeItem = RoomTypeItem;
		}
		
		public WebElement getTitle() {
			return RoomTypeItem.findElement(By.xpath("div[3]/div[1]/div[1]/h2"));
		}
		
		public WebElement getSelectButton() {
			return RoomTypeItem.findElement(By.xpath("div[3]/div[3]/div[2]/button"));
		}
		
		public WebElement getSelectRateTitle() {
			return RoomTypeItem.findElement(By.xpath("div[5]/div[1]/div[3]/div[1]"));
		}
		
		public WebElement getViewAllRates() {
			return RoomTypeItem.findElement(By.xpath("div[5]/div[1]/div[5]"));
		}
		
		public List<WebElement> getRateItems() {
			return RoomTypeItem.findElements(By.className("gbw-p-s-rate-list-item"));
		}
				
		public class RateItem {
			
			private WebElement RateItem;
			
			public RateItem(WebElement RateItem) {
				this.RateItem = RateItem;
			}
			
			public WebElement getTitle() {
				return RateItem.findElement(By.xpath("div[1]/div[1]/div[1]/div/h4"));
			}
			
			public WebElement getRadioButton() {
				return RateItem.findElement(By.xpath("div[1]/div[2]/div[4]/span[2]"));
			}
			
			//public WebElement getCheckoutButton() {
			//	return RateItem.findElement(By.xpath("//span[2]/button"));
			//}
		}
	}
	
	public WebElement getCheckoutButton() {
		return driver.findElement(By.xpath("//span[2]/button"));
	}
}
