package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class BlogPage {
public static WebDriver driver = null;
	
	public BlogPage(WebDriver driver){
		BlogPage.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBys({
		@FindBy(className = "wgt-breadcrumb"),
		@FindBy(tagName = "a")
	})
	private WebElement BreadcrumbLink;
	public WebElement getBreadcrumbLink()
	{
		return BreadcrumbLink;
	}
	
	@FindBy(css = "h3")
	private WebElement HeroTitle;
	public WebElement getHeroTitle()
	{
		return HeroTitle;
	}
	
	@FindBy(className = "wgt-sqbutton")
	private WebElement LearnMore;
	public WebElement getLearnMore()
	{
		return LearnMore;
	}
	
	@FindBy(className = "wgt-scrolldown")
	private WebElement ScrollToExplore;
	public WebElement getScrollToExplore()
	{
		return ScrollToExplore;
	}
	
	@FindBy(xpath = "//fieldset/div")
	private WebElement Location;
	public WebElement getLocation()
	{
		return Location;
	}
	
	@FindBy(xpath = "//fieldset/ul/li")
	private List<WebElement> LocationItems;
	public List<WebElement> getLocationItems()
	{
		return LocationItems;
	}
	
	@FindBy(xpath = "//div[2]/ul/li")
	private List<WebElement> BlogItems;
	public List<WebElement> getBlogItems()
	{
		return BlogItems;
	}
	
	public class BlogItem {
		private WebElement BlogItem;
		
		public BlogItem(WebElement BlogItem) {
			this.BlogItem = BlogItem;
		}
		
		public WebElement getSelf()
		{
			return BlogItem;
		}
		
		public WebElement getTitle()
		{
			return BlogItem.findElement(By.tagName("h3"));
		}
		
		public WebElement getExploreButton()
		{
			return BlogItem.findElement(By.className("wgt-sqbutton"));
		}
	}
	
	@FindBy(className = "wgt-readmore")
	private WebElement LoadMore;
	public WebElement getLoadMore()
	{
		return LoadMore;
	}
}
