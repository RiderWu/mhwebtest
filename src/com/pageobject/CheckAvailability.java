package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class CheckAvailability {
public WebDriver driver = null;
	
	public CheckAvailability(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "textfield-input")
	private WebElement SearchBox;
	public WebElement getSearchBox()
	{
		return SearchBox;
	}
	
	@FindBys({
		@FindBy(className = "search-list"),
		@FindBy(tagName = "li")
	})
	private List<WebElement> SearchListItems;
	public List<WebElement> getSearchListItems()
	{
		return SearchListItems;
	}
	
	@FindBy(className = "date-picker")
	private List<WebElement> DatePicker;
	public WebElement getCheckIn()
	{
		return DatePicker.get(0);
	}
	public WebElement getCheckOut()
	{
		return DatePicker.get(1);
	}
	
	@FindBy(className = "x-solar-icon-prev")
	private WebElement DatePickerPrevious;
	public WebElement getDatePickerPrevious()
	{
		return DatePickerPrevious;
	}
	
	@FindBy(className = "x-solar-icon-next")
	private WebElement DatePickerNext;
	public WebElement getDatePickerNext()
	{
		return DatePickerNext;
	}
	
	@FindBy(className = "x-solar-text")
	private List<WebElement> DateItems;
	public List<WebElement> getDateItems()
	{
		return DateItems;
	}
	
	@FindBy(xpath = "//div/div[2]/div/div/a")
	private WebElement CheckAvailabilityButton;
	public WebElement getCheckAvailabilityButton()
	{
		return CheckAvailabilityButton;
	}
	
	@FindBy(xpath = "//div/div[2]/div[3]/a")
	private WebElement CheckAvailabilityButtonOnTop;
	public WebElement getCheckAvailabilityButtonOnTop()
	{
		return CheckAvailabilityButtonOnTop;
	}
}
