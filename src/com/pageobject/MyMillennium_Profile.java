package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyMillennium_Profile {
	
	public WebDriver driver = null;
	
	public MyMillennium_Profile(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "accordion-trigger")
	private List<WebElement> ManageItems;
	public List<WebElement> getManageItems()
	{
		return ManageItems;
	}
}
