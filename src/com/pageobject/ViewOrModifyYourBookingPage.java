package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ViewOrModifyYourBookingPage {
	
	public WebDriver driver = null;
	
	public ViewOrModifyYourBookingPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(name = "last_name")
	private WebElement LastName;
	public WebElement getLastName()
	{
		return LastName;
	}
	
	@FindBy(name = "confirmation_number")
	private WebElement ConfirmationNumber;
	public WebElement getConfirmationNumber()
	{
		return ConfirmationNumber;
	}
	
	@FindBy(xpath = "//form/div/div[3]/a")
	private WebElement ViewBooking;
	public WebElement getViewBooking()
	{
		return ViewBooking;
	}
	
}
