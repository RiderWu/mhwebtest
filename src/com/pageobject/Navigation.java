package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Navigation {
public WebDriver driver = null;
	
	public Navigation(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//a/div")
	private WebElement Logo;
	public WebElement getLogo()
	{
		return Logo;
	}
	
	@FindBy(className = "item-hotel")
	private WebElement Hotels;
	public WebElement getHotels()
	{
		return Hotels;
	}
	
	@FindBy(className = "item-dest")
	private WebElement Destinations;
	public WebElement getDestinations()
	{
		return Destinations;
	}
	
	@FindBy(className = "item-offer")
	private WebElement Offers;
	public WebElement getOffers()
	{
		return Offers;
	}
	
	@FindBy(className = "item-coll")
	private WebElement Tours;
	public WebElement getTours()
	{
		return Tours;
	}
	
	@FindBy(className = "item-wedding")
	private WebElement Weddings;
	public WebElement getWeddings()
	{
		return Weddings;
	}
	
	@FindBy(className = "item-meetting")
	private WebElement Meetings;
	public WebElement getMeetings()
	{
		return Meetings;
	}
	
	@FindBy(className = "item-blog")
	private WebElement Blog;
	public WebElement getBlog()
	{
		return Blog;
	}
	
}
