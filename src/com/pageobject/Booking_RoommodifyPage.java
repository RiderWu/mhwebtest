package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Booking_RoommodifyPage {
	
	public WebDriver driver = null;
	
	public Booking_RoommodifyPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "bookingitem-edit")
	private WebElement EditButton;
	public WebElement getEditButton()
	{
		return EditButton;
	}
	
	@FindBy(className = "save")
	private WebElement SaveButton;
	public WebElement getSaveButton()
	{
		return SaveButton;
	}
	
	@FindBy(xpath = "//div[@id='review-wrapper']/div[2]/div/div[2]/div[5]/a")
	private WebElement NextStepButton;
	public WebElement getNextStepButton()
	{
		return NextStepButton;
	}
	
	@FindBy(className = "bookingitem-remove")
	private WebElement CancelButton;
	public WebElement getCancelButton()
	{
		return CancelButton;
	}
	
	@FindBy(className = "wgt-sqbutton-black")
	private WebElement CancelBookingButton;
	public WebElement getCancelBookingButton()
	{
		return CancelBookingButton;
	}
	
}
