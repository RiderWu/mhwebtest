package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyMillennium_Checkout_GuestsInfoPage {
	
	public WebDriver driver = null;
	
	public MyMillennium_Checkout_GuestsInfoPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[2]/div[3]/div/div")
	private WebElement PayWithCreditCard;
	public WebElement getPayWithCreditCard()
	{
		return PayWithCreditCard;
	}
	
	@FindBy(xpath = "//div[3]/div/div/div/i")
	private WebElement CreditCardArrow;
	public WebElement getCreditCardArrow()
	{
		return CreditCardArrow;
	}
	
	@FindBy(xpath = "//div[3]/div/div/div[2]/div/div/div/div/input")
	private WebElement CreditCardholdersName;
	public WebElement getCreditCardholdersName()
	{
		return CreditCardholdersName;
	}
	
	@FindBy(xpath = "//div[3]/div/div/div[2]/div/div/div[2]/div/input")
	private WebElement CreditCardNumber;
	public WebElement getCreditCardNumber()
	{
		return CreditCardNumber;
	}
	
	@FindBy(xpath = "//div[3]/div/div/input")
	private WebElement CreditCardSecurityCode;
	public WebElement getCreditCardSecurityCode()
	{
		return CreditCardSecurityCode;
	}
	
	@FindBy(xpath = "//div[3]/div[2]/div/div[1]/select")
	private WebElement CreditCardExpiryMonth;
	public WebElement getCreditCardExpiryMonth()
	{
		return CreditCardExpiryMonth;
	}
	
	@FindBy(xpath = "//div[3]/div[2]/div/div[2]/select")
	private WebElement CreditCardExpiryYear;
	public WebElement getCreditCardExpiryYear()
	{
		return CreditCardExpiryYear;
	}
	
	@FindBy(xpath = "//div[2]/div[4]/div/div")
	private WebElement OtherPaymentMethods;
	public WebElement getOtherPaymentMethods()
	{
		return OtherPaymentMethods;
	}
	
	@FindBy(xpath = "//div[4]/div/div[1]/div[1]/i")
	private WebElement OtherPaymentArrow;
	public WebElement getOtherPaymentArrow()
	{
		return OtherPaymentArrow;
	}
	
	@FindBy(xpath = "//div[4]/div/div[2]/div[1]/i")
	private WebElement PayWithChinaUnionPay;
	public WebElement getPayWithChinaUnionPay()
	{
		return PayWithChinaUnionPay;
	}
	
	@FindBy(xpath = "//div[4]/div/div[3]/div[1]/i")
	private WebElement PayWithAlipay;
	public WebElement getPayWithAlipay()
	{
		return PayWithAlipay;
	}
	
	@FindBy(className = "wgt-sqbutton-blue")
	private WebElement BookNowButton;
	public WebElement getBookNowButton()
	{
		return BookNowButton;
	}
	
	@FindBy(xpath = "//div/div/div/a[2]/span")
	private WebElement RemoveLink;
	public WebElement getRemoveLink()
	{
		return RemoveLink;
	}
	
	@FindBy(xpath = "//div[@id='removeConfirmWrapper']/div/div/a[2]")
	private WebElement RemoveButton;
	public WebElement getRemoveButton()
	{
		return RemoveButton;
	}
	
	@FindBy(xpath = "//div[@id='removeConfirmWrapper']/div/div/a")
	private WebElement CancelButton;
	public WebElement getCancelButton()
	{
		return CancelButton;
	}
}
