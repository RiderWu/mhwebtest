package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class KP_ActiveAndFitPage {
public static WebDriver driver = null;
	
	public KP_ActiveAndFitPage(WebDriver driver){
		KP_ActiveAndFitPage.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(tagName = "header")
	private WebElement Header;
	public WebElement getHeader()
	{
		return Header;
	}
	
	@FindBy(className = "l-desktop")
	private WebElement MarqueeImageDesktop;
	public WebElement getMarqueeImageDesktop()
	{
		return MarqueeImageDesktop;
	}
	
	@FindBy(className = "l-mobile")
	private WebElement MarqueeImageMobile;
	public WebElement getMarqueeImageMobile()
	{
		return MarqueeImageMobile;
	}
	
	@FindBy(className = "headline-holder")
	private WebElement MarqueeOverlapText;
	public WebElement getMarqueeOverlapText()
	{
		return MarqueeOverlapText;
	}
	
	/*
	@FindBy(xpath = "//*[@id='maincontent']/div/div[1]/div[2]/span")
	private WebElement MarqueeLearnMoreText;
	public WebElement getMarqueeLearnMoreText()
	{
		return MarqueeLearnMoreText;
	}
	*/
	
	@FindBys({
		@FindBy(className = "gb-bread-crumbs"),
		@FindBy(tagName = "li")
	})
	private List<WebElement> BreadcrumbNavigationItems;
	public List<WebElement> getBreadcrumbNavigationItems()
	{
		return BreadcrumbNavigationItems;
	}
	
	@FindBy(id = "kp-main")
	private WebElement GymIntroductionTitle;
	public WebElement getGymIntroductionTitle()
	{
		return GymIntroductionTitle;
	}
	
	@FindBys({
		@FindBy(className = "intro-section"),
		@FindBy(tagName = "h2")
	})
	private WebElement GymIntroductionSubTitle;
	public WebElement getGymIntroductionSubTitle()
	{
		return GymIntroductionSubTitle;
	}
	
	@FindBy(className = "intro")
	private WebElement GymIntroductionIntro;
	public WebElement getGymIntroductionIntro()
	{
		return GymIntroductionIntro;
	}
	
	@FindBys({
		@FindBy(className = "kiwi-grid"),
		@FindBy(tagName = "img")
	})
	private List<WebElement> GymSectionImages;
	public List<WebElement> getGymSectionImages()
	{
		return GymSectionImages;
	}
	
	@FindBys({
		@FindBy(className = "kiwi-grid"),
		@FindBy(className = "title")
	})
	private List<WebElement> GymSectionTitles;
	public List<WebElement> getGymSectionTitles()
	{
		return GymSectionTitles;
	}
	
	@FindBys({
		@FindBy(className = "kiwi-grid"),
		@FindBy(className = "seperator")
	})
	private List<WebElement> GymSectionSeperators;
	public List<WebElement> getGymSectionSeperators()
	{
		return GymSectionSeperators;
	}
	
	@FindBys({
		@FindBy(className = "kiwi-grid"),
		@FindBy(className = "text-holder")
	})
	private List<WebElement> GymSectionTexts;
	public List<WebElement> getGymSectionTexts()
	{
		return GymSectionTexts;
	}
	
	@FindBys({
		@FindBy(className = "articles-section"),
		@FindBy(tagName = "img")
	})
	private List<WebElement> SerpentineImages;
	public List<WebElement> getSerpentineImages()
	{
		return SerpentineImages;
	}
	
	@FindBys({
		@FindBy(className = "articles-section"),
		@FindBy(className = "title")
	})
	private List<WebElement> SerpentineTitles;
	public List<WebElement> getSerpentineTitles()
	{
		return SerpentineTitles;
	}
	
	@FindBys({
		@FindBy(className = "articles-section"),
		@FindBy(className = "seperator")
	})
	private List<WebElement> SerpentineSeperators;
	public List<WebElement> getSerpentineSeperators()
	{
		return SerpentineSeperators;
	}
	
	@FindBys({
		@FindBy(className = "articles-section"),
		@FindBy(className = "content")
	})
	private List<WebElement> SerpentineTexts;
	public List<WebElement> getSerpentineTexts()
	{
		return SerpentineTexts;
	}
	
	@FindBys({
		@FindBy(className = "social-section"),
		@FindBy(tagName = "h3")
	})
	private WebElement SocialSharingTitle;
	public WebElement getSocialSharingTitle()
	{
		return SocialSharingTitle;
	}
	
	@FindBy(className = "facebook")
	private WebElement SocialSharingFacebook;
	public WebElement getSocialSharingFacebook()
	{
		return SocialSharingFacebook;
	}
	
	@FindBy(className = "twitter")
	private WebElement SocialSharingTwitter;
	public WebElement getSocialSharingTwitter()
	{
		return SocialSharingTwitter;
	}
	
	@FindBy(className = "instagram")
	private WebElement SocialSharingInstagram;
	public WebElement getSocialSharingInstagram()
	{
		return SocialSharingInstagram;
	}
	
	@FindBy(className = "note-section")
	private WebElement FootNote;
	public WebElement getFootNote()
	{
		return FootNote;
	}
	
	@FindBy(className = "v1")
	private WebElement Footer;
	public WebElement getFooter()
	{
		return Footer;
	}
}
