package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyMillennium_SignUp {
	
	public WebDriver driver = null;
	
	public MyMillennium_SignUp(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[4]/a")
	private WebElement RegisterNow;
	public WebElement getRegisterNow()
	{
		return RegisterNow;
	}
}
