package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HotelsPage {
public WebDriver driver = null;
	
	public HotelsPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[2]/div[1]/div[1]/ul/li/a")
	private List<WebElement> Top10Hotels;
	public List<WebElement> getTop10Hotels()
	{
		return Top10Hotels;
	}
	
	@FindBy(xpath = "//div[2]/ul/li/a")
	private WebElement Beijing;
	public WebElement getBeijing()
	{
		return Beijing;
	}
	
	@FindBy(xpath = "//div[2]/ul/li[4]/a")
	private WebElement Chengdu;
	public WebElement getChengdu()
	{
		return Chengdu;
	}
	
	@FindBy(xpath = "//h4/a")
	private WebElement Singapore;
	public WebElement getSingapore()
	{
		return Singapore;
	}
	
	@FindBy(xpath = "//div[21]/ul/li/a")
	private WebElement London;
	public WebElement getLondon()
	{
		return London;
	}
	
	@FindBy(xpath = "//div[22]/ul/li[21]/a")
	private WebElement NewYork;
	public WebElement getNewYork()
	{
		return NewYork;
	}
	
	@FindBy(xpath = "//div[17]/ul/li/a")
	private WebElement CopthorneKingsHotel;
	public WebElement getCopthorneKingsHotel()
	{
		return CopthorneKingsHotel;
	}
	
	@FindBy(xpath = "//div[2]/ul/li[3]/a")
	private WebElement MillenniumResidencesBeijingFortunePlaza;
	public WebElement getMillenniumResidencesBeijingFortunePlaza()
	{
		return MillenniumResidencesBeijingFortunePlaza;
	}
	
	@FindBy(xpath = "//div[2]/ul/li[5]/a")
	private WebElement MHotelChengdu;
	public WebElement getMHotelChengdu()
	{
		return MHotelChengdu;
	}
	
	@FindBy(className = "to-destination")
	private WebElement ViewDestinationMap;
	public WebElement getViewDestinationMap()
	{
		return ViewDestinationMap;
	}
}
