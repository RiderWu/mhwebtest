package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class Footer {
public WebDriver driver = null;
	
	public Footer(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "nav-1")
	private WebElement LengsCollection;
	public WebElement getLengsCollection()
	{
		return LengsCollection;
	}
	
	@FindBy(className = "nav-2")
	private WebElement MillenniumCollection;
	public WebElement getMillenniumCollection()
	{
		return MillenniumCollection;
	}
	
	@FindBy(className = "nav-3")
	private WebElement CopthorneCollection;
	public WebElement getCopthorneCollection()
	{
		return CopthorneCollection;
	}
	
	@FindBy(className = "facebook-icon")
	private WebElement Facebook;
	public WebElement getFacebook()
	{
		return Facebook;
	}
	
	@FindBy(className = "twitter-icon")
	private WebElement Twitter;
	public WebElement getTwitter()
	{
		return Twitter;
	}
	
	@FindBy(className = "weibo-icon")
	private WebElement Weibo;
	public WebElement getWeibo()
	{
		return Weibo;
	}
	
	@FindBy(id = "subscribe_input")
	private WebElement NewsletterBox;
	public WebElement getNewsletterBox()
	{
		return NewsletterBox;
	}
	
	@FindBy(className = "subs-btn")
	private WebElement SubscribeButton;
	public WebElement getSubscribeButton()
	{
		return SubscribeButton;
	}
	
	@FindBys({
		@FindBy(className = "show-hide"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> FooterLinkItems;
	public List<WebElement> getFooterLinkItems()
	{
		return FooterLinkItems;
	}
	
	@FindBy(xpath = "//*[@id='main-footer-wrapper']/section/div[3]/div[2]/div/div[2]/a[1]")
	private WebElement TermsOfUse;
	public WebElement getTermsOfUse()
	{
		return TermsOfUse;
	}
	
	@FindBy(xpath = "//*[@id='main-footer-wrapper']/section/div[3]/div[2]/div/div[2]/a[3]")
	private WebElement Sitemap;
	public WebElement getSitemap()
	{
		return Sitemap;
	}
	
	@FindBy(xpath = "//*[@id='main-footer-wrapper']/section/div[3]/div[2]/div/div[2]/a[5]")
	private WebElement CookiePolicyAndDataSecurityNotice ;
	public WebElement getCookiePolicyAndDataSecurityNotice()
	{
		return CookiePolicyAndDataSecurityNotice;
	}
	
	@FindBy(xpath = "//*[@id='main-footer-wrapper']/section/div[3]/div[2]/div/div[2]/a[7]")
	private WebElement SupplyChainTransparencyStatement ;
	public WebElement getSupplyChainTransparencyStatement()
	{
		return SupplyChainTransparencyStatement;
	}
}
