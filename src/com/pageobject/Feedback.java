package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Feedback {
public WebDriver driver = null;
	
	public Feedback(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "usabilla_live_button_container")
	private WebElement FeedbackButton;
	public WebElement getFeedbackButton()
	{
		return FeedbackButton;
	}
	
	@FindBy(xpath = "//body[@id='usabilla-metadata-container']/div/form/a")
	private WebElement FeedbackClose;
	public WebElement getFeedbackClose()
	{
		return FeedbackClose;
	}
}
