package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyMillennium_Checkout_ConfirmationPage {
	
	public WebDriver driver = null;
	
	public MyMillennium_Checkout_ConfirmationPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "number")
	private WebElement ConfirmationNumber;
	public WebElement getConfirmationNumber()
	{
		return ConfirmationNumber;
	}
	
	@FindBy(xpath = "//div/ul/li[1]/span")
	private WebElement CustomerName;
	public WebElement getCustomerName()
	{
		return CustomerName;
	}
	
	@FindBy(className = "hotel-name")
	private WebElement HotelName;
	public WebElement getHotelName()
	{
		return HotelName;
	}
	
	@FindBy(xpath = "//div/div[2]/a")
	private WebElement UpgradesImage;
	public WebElement getUpgradesImage()
	{
		return UpgradesImage;
	}
	
}
