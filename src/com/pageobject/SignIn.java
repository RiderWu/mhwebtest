package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignIn {
public WebDriver driver = null;
	
	public SignIn(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindAll({
		@FindBy(xpath = "//div[10]/div/div[2]/div/div/a"),
		@FindBy(xpath = "//div[9]/div/div[2]/div/div/a"),
		@FindBy(xpath = "//div[8]/div/div[2]/div/div/a")
		})
	private WebElement Avatar;
	public WebElement getAvatar()
	{
		return Avatar;
	}
	
	@FindAll({
		@FindBy(xpath = "//div[10]/div/div[2]/div/div[2]"),
		@FindBy(xpath = "//div[9]/div/div[2]/div/div[2]"),
		@FindBy(xpath = "//div[8]/div/div[2]/div/div[2]")
		})
	private WebElement SignIn;
	public WebElement getSignIn()
	{
		return SignIn;
	}
	
	@FindBy(css = "div.user-module > div.nav > div.logged-in > div.login-in > div.user-hello > a.user-info.always-show-image")
	private WebElement SignInUser;
	public WebElement getSignInUser()
	{
		return SignInUser;
	}
	
	@FindBy(className = "wgt-sqbutton-facebook")
	private WebElement SignOnFacebook;
	public WebElement getSignOnFacebook()
	{
		return SignOnFacebook;
	}
	
	@FindBy(className = "wgt-sqbutton-google")
	private WebElement SignOnGoogle;
	public WebElement getSignOnGoogle()
	{
		return SignOnGoogle;
	}
	
	@FindBy(id = "opt-sign-up")
	private WebElement RegisterNow;
	public WebElement getRegisterNow()
	{
		return RegisterNow;
	}
	
	@FindBy(id = "login_username")
	private WebElement Username;
	public WebElement getUsername()
	{
		return Username;
	}
	
	@FindBy(id = "login_password")
	private WebElement Password;
	public WebElement getPassword()
	{
		return Password;
	}
	
	@FindBy(className = "checkbox")
	private WebElement RememberMe;
	public WebElement getRememberMe()
	{
		return RememberMe;
	}
	
	@FindBy(className = "forget-pssd")
	private WebElement ForgetPassword;
	public WebElement getForgetPassword()
	{
		return ForgetPassword;
	}
	
	@FindBy(className = "signin")
	private WebElement SignInButton;
	public WebElement getSignInButton()
	{
		return SignInButton;
	}
	
	@FindBy(id = "opt-view-profile")
	private WebElement ViewProfile;
	public WebElement getViewProfile()
	{
		return ViewProfile;
	}
	
	@FindBy(xpath = "(//a[@id='opt-logout'])[2]")
	private WebElement SignOut;
	public WebElement getSignOut()
	{
		return SignOut;
	}
	
	@FindBy(id = "shopping-cart")
	private WebElement ShoppingCart;
	public WebElement getShoppingCart()
	{
		return ShoppingCart;
	}
	
	@FindAll({
		@FindBy(xpath = "//div[10]/div/div[3]/div/div[2]"),
		@FindBy(xpath = "//div[9]/div/div[3]/div/div[2]"),
		@FindBy(xpath = "//div[8]/div/div[3]/div/div[2]")
		})
	private WebElement Currency;
	public WebElement getCurrency()
	{
		return Currency;
	}
	
	@FindAll({
		@FindBy(xpath = "//div[10]/div/div[3]/div/div[3]"),
		@FindBy(xpath = "//div[9]/div/div[3]/div/div[3]"),
		@FindBy(xpath = "//div[8]/div/div[3]/div/div[3]")
		})
	private WebElement Language;
	public WebElement getLanguage()
	{
		return Language;
	}
}
