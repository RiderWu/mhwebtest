package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ScrollButtons {
public WebDriver driver = null;
	
	public ScrollButtons(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "wgt-scrolldown")
	private WebElement ScrollDown;
	public WebElement getScrollDown()
	{
		return ScrollDown;
	}
	
	@FindBy(className = "backtotop-button")
	private WebElement BackToTop;
	public WebElement getBackToTop()
	{
		return BackToTop;
	}
}
