package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
public WebDriver driver = null;
	
	public HomePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[4]/div")
	private List<WebElement> HalfReviews;
	public List<WebElement> getHalfReviews()
	{
		return HalfReviews;
	}
	
	@FindBy(xpath = "//div[4]/div/div/div[3]/a")
	private WebElement ExploreParisDiscoverButton;
	public WebElement getExploreParisDiscoverButton()
	{
		return ExploreParisDiscoverButton;
	}
	
	@FindBy(xpath = "//div[2]/div/div[3]/a")
	private WebElement ViewOfferButton;
	public WebElement getViewOfferButton()
	{
		return ViewOfferButton;
	}
	
	@FindBy(xpath = "//div[3]/div/div[3]/a")
	private WebElement HelloWeekendDiscoverButton;
	public WebElement getHelloWeekendDiscoverButton()
	{
		return HelloWeekendDiscoverButton;
	}
	
	@FindBy(xpath = "//div[4]/div/div[3]/a")
	private WebElement ViewAllReviewsButton;
	public WebElement getViewAllReviewsButton()
	{
		return ViewAllReviewsButton;
	}
	
	@FindBy(xpath = "//div[5]/div/div[3]/a")
	private WebElement LearnMoreButton;
	public WebElement getLearnMoreButton()
	{
		return LearnMoreButton;
	}
	
	@FindBy(xpath = "//div[6]/div/div[3]/a")
	private WebElement ExploreNewYorkDiscoverButton;
	public WebElement getExploreNewYorkDiscoverButton()
	{
		return ExploreNewYorkDiscoverButton;
	}
}
