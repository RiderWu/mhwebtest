package com.pageobject;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class DestinationsPage {
public WebDriver driver = null;
	
	public DestinationsPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBys({
		@FindBy(className = "location"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Regions;
	public List<WebElement> getRegions()
	{
		return Regions;
	}
	
	@FindBy(className = "icon-type-back")
	private WebElement Back;
	public WebElement getBack()
	{
		return Back;
	}
	
	@FindBy(xpath = "//div[2]/div/div/div/div/ul/li/a")
	private List<WebElement> Countries;
	public List<WebElement> getCountries()
	{
		return Countries;
	}
	
	@FindBys({
		@FindBy(className = "breadcrumb"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Breadcrumb;
	public List<WebElement> getBreadcrumb()
	{
		return Breadcrumb;
	}
	
	@FindBy(xpath = "//div[2]/ul/li/a")
	private List<WebElement> Hotels;
	public List<WebElement> getHotels()
	{
		return Hotels;
	}
	
	@FindBy(className = "desktop")
	private WebElement ViewDestinationList;
	public WebElement getViewDestinationList()
	{
		return ViewDestinationList;
	}
	
	@FindBy(className = "to-top")
	private WebElement ViewDestinationMap;
	public WebElement getViewDestinationMap()
	{
		return ViewDestinationMap;
	}
	
	@FindBy(className = "tpl-hero-slider")
	private WebElement HotelHeroImage;
	public WebElement getHotelHeroImage()
	{
		return HotelHeroImage;
	}
	
	@FindBy(className = "title")
	private WebElement HotelNameTitle;
	public WebElement getHotelNameTitle()
	{
		return HotelNameTitle;
	}
	
	@FindBy(xpath = "//div[@id='DestinationInformation']/div/ul/li/div[2]/div/div/div/div/div/div/a/span")
	private WebElement Favourite;
	public WebElement getFavourite()
	{
		return Favourite;
	}
	
	@FindBy(xpath = "//div/div/div/div/div/div[2]/div/a")
	private WebElement SeeReviews;
	public WebElement getSeeReviews()
	{
		return SeeReviews;
	}
	
	@FindBy(xpath = "//li/div[3]/a")
	private WebElement HotelDetails;
	public WebElement getHotelDetails()
	{
		return HotelDetails;
	}
	
	@FindBy(xpath = "//li/div[3]/a[2]")
	private WebElement BookNow;
	public WebElement getBookNow()
	{
		return BookNow;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[1]")
	private WebElement HotelView;
	public WebElement getHotelView()
	{
		return HotelView;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[2]")
	private WebElement NearbyAttractions;
	public WebElement getNearbyAttractions()
	{
		return NearbyAttractions;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[2]/div[2]/div/div/div/ul/li")
	private List<WebElement> AttractionsList;
	public List<WebElement> getAttractionsList()
	{
		return AttractionsList;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[2]/div[2]/div/div/div/div[2]/div/div/a")
	private WebElement ViewNearestHotel;
	public WebElement getViewNearestHotel()
	{
		return ViewNearestHotel;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[3]")
	private WebElement NearbyTours;
	public WebElement getNearbyTours()
	{
		return NearbyTours;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[3]/div[2]/div[1]/div/div/ul/li")
	private List<WebElement> ToursList;
	public List<WebElement> getToursList()
	{
		return ToursList;
	}
	
	@FindBy(xpath = "//div[@id='DestinationInformation']/div/ul/li[3]/div[2]/div/div/div/div[2]/div/a/span")
	private WebElement TourFavourite;
	public WebElement getTourFavourite()
	{
		return TourFavourite;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[3]/div[2]/div[1]/div/div/div[2]/div[1]/div/div[3]/a")
	private WebElement SeeTourReviews;
	public WebElement getSeeTourReviews()
	{
		return SeeTourReviews;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[3]/div[2]/div[1]/div/div/div[2]/div[1]/p/a")
	private WebElement MoreDetails;
	public WebElement getMoreDetails()
	{
		return MoreDetails;
	}
	
	@FindBy(xpath = "//*[@id='DestinationInformation']/div/ul/li[3]/div[2]/div[1]/div/div/div[2]/div[2]/a")
	private WebElement BookTourNow;
	public WebElement getBookTourNow()
	{
		return BookTourNow;
	}
	
	@FindBy(className = "icon-icon_B017")
	private WebElement CloseButton;
	public WebElement getCloseButton()
	{
		return CloseButton;
	}
	
	@FindBy(xpath = "//div[2]/div[1]/div[1]/ul/li/a")
	private List<WebElement> Top10Destinations;
	public List<WebElement> getTop10Destinations()
	{
		return Top10Destinations;
	}
	
}
