package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class OffersPage {
public static WebDriver driver = null;
	
	public OffersPage(WebDriver driver){
		OffersPage.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "destination-filter")
	private WebElement DestinationFilter;
	public WebElement getDestinationFilter()
	{
		return DestinationFilter;
	}
	
	@FindBys({
		@FindBy(className = "contents"),
		@FindBy(tagName = "h4")
	})
	private List<WebElement> Countries;
	public List<WebElement> getCountries()
	{
		return Countries;
	}
	
	@FindBys({
		@FindBy(className = "contents"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Cities;
	public List<WebElement> getCities()
	{
		return Cities;
	}
	
	@FindBy(className = "select-root")
	private WebElement HotelFilter;
	public WebElement getHotelFilter()
	{
		return HotelFilter;
	}
	
	@FindBys({
		@FindBy(className = "scroll-wrapper"),
		@FindBy(tagName = "li")
	})
	private List<WebElement> Hotels;
	public List<WebElement> getHotels()
	{
		return Hotels;
	}
	
	@FindBy(css = "a.clear-all.item")
	private WebElement ClearAll;
	public WebElement getClearAll()
	{
		return ClearAll;
	}
	
	@FindBys({
		@FindBy(className = "sidebar-destination"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> Top10Destinations;
	public List<WebElement> getTop10Destinations()
	{
		return Top10Destinations;
	}
	
	@FindBys({
		@FindBy(className = "sidebar-hotel"),
		@FindBy(tagName = "h6")
	})
	private List<WebElement> PopularHotels;
	public List<WebElement> getPopularHotels()
	{
		return PopularHotels;
	}
	
	@FindBys({
		@FindBy(className = "sidebar-hotel"),
		@FindBy(tagName = "a")
	})
	private List<WebElement> HotelDetailsButtons;
	public List<WebElement> getHotelDetailsButtons()
	{
		return HotelDetailsButtons;
	}
	
	@FindBy(className = "wgt-readmore")
	private WebElement LoadMoreOffers;
	public WebElement getLoadMoreOffers()
	{
		return LoadMoreOffers;
	}
	
	@FindBy(className = "tpl-half-offer")
	private List<WebElement> Offers;
	public List<WebElement> getOffers()
	{
		return Offers;
	}
	
	public class Offer {
		
		private WebElement Offer;
		
		public Offer(WebElement Offer) {
			this.Offer = Offer;
		}
		
		public WebElement getTitle() {
			return Offer.findElement(By.xpath("div/div[2]/h3/span[1]"));
		}
		
		public WebElement getBookNow() {
			return Offer.findElement(By.xpath("div/div[2]/div/a"));
		}
		
		public WebElement getFavourite() {
			return Offer.findElement(By.xpath("div/div[2]/h3/a/span"));
		}
		
		public WebElement getSocialShare() {
			return Offer.findElement(By.xpath("div/div[2]/h3/span[2]"));
		}
	}
	
}
