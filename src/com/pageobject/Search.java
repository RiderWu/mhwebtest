package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Search {
public WebDriver driver = null;
	
	public Search(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "expand-trigger")
	private WebElement Search;
	public WebElement getSearch()
	{
		return Search;
	}
	
	/*
	@FindBy(xpath = "/html/body/div[3]/div[2]/div/div/div/div[2]/div/div[1]/input")
	private WebElement SearchBox;
	public WebElement getSearchBox()
	{
		return SearchBox;
	}
	*/
	
	@FindBy(className = "text")
	private WebElement Close;
	public WebElement getClose()
	{
		return Close;
	}
}
