package com.pageobject;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

public class MeetingsAndEventsPage {
public static WebDriver driver = null;
	
	public MeetingsAndEventsPage(WebDriver driver){
		MeetingsAndEventsPage.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(className = "wgt-scrolldown")
	private WebElement ScrollToExplore;
	public WebElement getScrollToExplore()
	{
		return ScrollToExplore;
	}
	
	@FindBy(xpath = "//fieldset/div")
	private WebElement Destinations;
	public WebElement getDestinations()
	{
		return Destinations;
	}
	
	@FindBy(xpath = "//fieldset/ul/li")
	private List<WebElement> DestinationsItems;
	public List<WebElement> getDestinationsItems()
	{
		return DestinationsItems;
	}
	
	@FindBy(xpath = "//div[2]/fieldset/div")
	private WebElement EventType;
	public WebElement getEventType()
	{
		return EventType;
	}
	
	@FindBy(xpath = "//div[2]/fieldset/ul/li")
	private List<WebElement> EventTypeItems;
	public List<WebElement> getEventTypeItems()
	{
		return EventTypeItems;
	}
	
	@FindBy(xpath = "//div[3]/fieldset/div")
	private WebElement Attendees;
	public WebElement getAttendees()
	{
		return Attendees;
	}
	
	@FindBy(xpath = "//div[3]/fieldset/ul/li")
	private List<WebElement> AttendeesItems;
	public List<WebElement> getAttendeesItems()
	{
		return AttendeesItems;
	}
	
	@FindBy(className = "image-loaded")
	private List<WebElement> ResultsWithoutFilter;
	public List<WebElement> getResultsWithoutFilter()
	{
		return ResultsWithoutFilter;
	}
	
	public class ResultWithoutFilter {
		private WebElement ResultWithoutFilter;
		
		public ResultWithoutFilter(WebElement ResultWithoutFilter) {
			this.ResultWithoutFilter = ResultWithoutFilter;
		}
		
		public WebElement getSelf()
		{
			return ResultWithoutFilter;
		}
		
		public WebElement getTitle()
		{
			return ResultWithoutFilter.findElement(By.className("tpl-text")).findElement(By.tagName("p"));
		}
		
		public WebElement getLearnMoreButton()
		{
			return ResultWithoutFilter.findElement(By.className("wgt-sqbutton"));
		}
		
	}
	
	@FindBy(xpath = "//div[2]/div/div/div[2]/a/span")
	private WebElement Facebook;
	public WebElement getFacebook()
	{
		return Facebook;
	}
	
	@FindBy(xpath = "//div[2]/a[2]/span")
	private WebElement Twitter;
	public WebElement getTwitter()
	{
		return Twitter;
	}
	
	@FindBy(xpath = "//div[2]/a[3]/span")
	private WebElement Weibo;
	public WebElement getWeibo()
	{
		return Weibo;
	}
	
	@FindBys({
		@FindBy(className = "results"),
		@FindBy(tagName = "li")
	})
	private List<WebElement> MeetingsAndEventsResults;
	public List<WebElement> getMeetingsAndEventsResults()
	{
		return MeetingsAndEventsResults;
	}
	
	public class MeetingsAndEventsResult{
		private WebElement MeetingsAndEventsResult;
		
		public MeetingsAndEventsResult(WebElement MeetingsAndEventsResult) {
			this.MeetingsAndEventsResult = MeetingsAndEventsResult;
		}
		
		public WebElement getSelf()
		{
			return MeetingsAndEventsResult;
		}
		
		public WebElement getTitle()
		{
			return MeetingsAndEventsResult.findElement(By.className("tpl-content")).findElement(By.tagName("h3"));
		}
		
		public WebElement getViewFeatures()
		{
			return MeetingsAndEventsResult.findElement(By.className("compare-button"));
		}
		
		public WebElement getFavourite()
		{
			return MeetingsAndEventsResult.findElement(By.className("tpl-content")).findElement(By.tagName("h3")).findElement(By.tagName("a"));
		}
		
		public WebElement getMoreInfoButton()
		{
			return MeetingsAndEventsResult.findElement(By.className("wgt-sqbutton"));
		}
	}
	
	@FindBy(className = "wgt-readmore")
	private WebElement LoadMore;
	public WebElement getLoadMore()
	{
		return LoadMore;
	}
}
