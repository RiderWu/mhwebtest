package com.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyMillennium_Scratchpad {
	
	public WebDriver driver = null;
	
	public MyMillennium_Scratchpad(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div[4]/ul/li/a")
	private WebElement ManageMyAccount;
	public WebElement getManageMyAccount()
	{
		return ManageMyAccount;
	}
	
	@FindBy(xpath = "//div[4]/ul/li[2]/a")
	private WebElement PointsHistory;
	public WebElement getPointsHistory()
	{
		return PointsHistory;
	}
	
	@FindBy(xpath = "//div[4]/ul/li[3]/a")
	private WebElement TravelPreferences;
	public WebElement getTravelPreferences()
	{
		return TravelPreferences;
	}
}
