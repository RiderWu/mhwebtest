package com.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class IxnrtaDialog {
public WebDriver driver = null;
	
	public IxnrtaDialog(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement getixnrtaDialog()
	{
		try{
			return driver.findElement(By.className("ixnrtaDisplayVisible"));
		}
		catch(NoSuchElementException ex){
			return null;
		}
	}
	
	public WebElement getBookButton()
	{
		try{
			return driver.findElement(By.className("ixnrtaDisplayVisible")).findElement(By.className("ixnrtaMainButton"));
		}
		catch(NoSuchElementException ex){
			return null;
		}
	}
	
	public WebElement getCloseButton()
	{
		try{
			return driver.findElement(By.className("ixnrtaDisplayVisible")).findElement(By.className("ixnrtaCloseButton"));
		}
		catch(NoSuchElementException ex){
			return null;
		}
	}
}
