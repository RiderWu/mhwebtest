package com.web;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pageobject.IxnrtaDialog;

public class Common {

	public static WebDriver driver = null;
	public static String osName = System.getProperties().getProperty("os.name");
	//public static String osArch = System.getProperties().getProperty("os.arch");

	public static WebDriver openBrowser(String browserName, String url, String width, String height) throws Exception {
		try {

			if (browserName.equalsIgnoreCase("FireFox")) {
				DesiredCapabilities capability = DesiredCapabilities.firefox();
				capability.setCapability("marionette", false);
				if (osName.toLowerCase().contains("win")) {
					System.setProperty("webdriver.gecko.driver", "res/geckodriver.exe");
				} else if (osName.toLowerCase().contains("mac")){
					System.setProperty("webdriver.gecko.driver", "res/mac/geckodriver");
				}
				driver = new FirefoxDriver(capability);
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (browserName.equalsIgnoreCase("Chrome")) {
				if (osName.toLowerCase().contains("win")) {
					System.setProperty("webdriver.chrome.driver", "res/chromedriver.exe");
				} else if (osName.toLowerCase().contains("mac")){
					System.setProperty("webdriver.chrome.driver", "res/mac/chromedriver");
				}
				driver = new ChromeDriver();
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			} else if (browserName.equalsIgnoreCase("IE")) {
				DesiredCapabilities capability = new DesiredCapabilities();
				capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
				capability.setCapability("ignoreProtectedModeSettings", true);
				System.setProperty("webdriver.ie.driver", "res/IEDriverServer.exe");
				driver = new InternetExplorerDriver(capability);
				//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				//driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
				driver.manage().window().setSize(new Dimension(Integer.parseInt(width),Integer.parseInt(height)));
				driver.get(url);

			}

		} catch (TimeoutException ignored) {
			System.out.println("Browser load timeout.");
			((JavascriptExecutor) driver).executeScript("window.stop()");
			return driver;
		} catch (Exception e) {
			System.out.println("Browser setting error.");
			e.printStackTrace();
			driver.quit();
			return null;
		}

		return driver;
	}

	public static void wait(int timeout, String xpath) {
		(new WebDriverWait(driver, timeout)).until(new ExpectedCondition<WebElement>() {
			@Override
			public WebElement apply(WebDriver d) {
				return d.findElement(By.xpath(xpath));
			}
		});
	}

	public static void scrollTo(int height) {
		((JavascriptExecutor) driver)
				.executeScript(String.format("window.scrollTo(0, %d)", height));
	}
	
	public static void scrollTo(WebElement element) {
		((JavascriptExecutor) driver)
				.executeScript(String.format("window.scrollTo(0, %d)", element.getLocation().getY()));
	}
	
	public static void swipeTo(WebElement element) {
		try {
			//int width = driver.manage().window().getSize().width;
			//int height = driver.manage().window().getSize().height;
			//driver.swipe(width / 2, height * 3 / 4, width / 2, height / 4, element.getLocation().getY());
			HashMap<String, Double> swipeObj = new HashMap<String, Double>();
			swipeObj.put("startX", 100.0);
			swipeObj.put("startY", 100.0);
			swipeObj.put("endX", 100.0);
			swipeObj.put("endY", 10.0);
			swipeObj.put("duration", 1.0);
			swipeObj.put("element", Double.valueOf(((RemoteWebElement)element).getId()));
			((JavascriptExecutor) driver)
					.executeScript("mobile: swipe", swipeObj);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void takeScreenShotMobile(WebDriver driver, WebElement element, String foldername, String timeStamp, String testCaseName,
			int number) throws Exception {
		try {

			String path = "screenshots/" + timeStamp + "/" + foldername + "/" + testCaseName + "_" + number + ".png";
			File file = new File(path);
			FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), file);

		} catch (Exception e) {
			throw (e);
		}

	}
	
	public static void takeScreenShot(WebDriver driver, WebElement element, String foldername, String timeStamp, String testCaseName,
			String index) throws Exception {
		try {
			
			String path = "/screenshots/" + timeStamp + "/" + foldername + "/" + testCaseName + "/" + index + ".png";
			File file = new File(path);
			
			//FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE), file);
			
			IxnrtaDialog ixnrtaDialog = new IxnrtaDialog(driver);
			if(null != ixnrtaDialog.getixnrtaDialog()&& null != ixnrtaDialog.getCloseButton()){
				ixnrtaDialog.getCloseButton().click();
				Thread.sleep(2000);
			}
			
			if(!file.exists()) file.mkdirs();
			BufferedImage image = new Robot().createScreenCapture(new Rectangle(driver.manage().window().getPosition().getX(),driver.manage().window().getPosition().getY(),driver.manage().window().getSize().getWidth(),driver.manage().window().getSize().getHeight()));
			ImageIO.write(image, "png", file);
			
			if(null != element) {
				int LocationY = 0;
				if(foldername.startsWith("Chrome"))LocationY = 0;
				else if(foldername.startsWith("FireFox")||foldername.startsWith("IE"))LocationY = element.getLocation().getY();
				Rectangle area = new Rectangle(0, LocationY, driver.manage().window().getSize().getWidth(), driver.manage().window().getSize().getHeight());
				cutImage(file,path,area);
			}

		} catch (Exception e) {
			throw (e);
		}

	}
	
	public static void ScrollDownAndTakeScreenShot(WebDriver driver, WebElement element, String foldername, String timeStamp, String testCaseName,
			String index) throws Exception {
		try {
			
			int h = -100,count=1;
			
			Common.takeScreenShot(driver, null, foldername, timeStamp, testCaseName, 
					index+count);
			count++;
			
			while (h < driver.findElement(By.xpath("//body")).getSize().getHeight()-driver.manage().window().getSize().getHeight()-100 && driver.manage().window().getSize().getHeight() != driver.findElement(By.xpath("/html")).getSize().getHeight()) {
				//action.sendKeys(Keys.PAGE_DOWN).perform();
				Common.scrollTo(h+driver.manage().window().getSize().getHeight());
				Thread.sleep(10000);
				Common.takeScreenShot(driver, null, foldername, timeStamp, testCaseName, 
						index+count);
				h += (driver.manage().window().getSize().getHeight()-100);
				count++;
			}

		} catch (Exception e) {
			throw (e);
		}
	}
	
	public static void cutImage(File srcImg, String destImgPath, java.awt.Rectangle rect){
        File destImg = new File(destImgPath);
        if(destImg.exists()){
            String p = destImg.getPath();
            try {
                if(!destImg.isDirectory()) p = destImg.getParent();
                if(!p.endsWith(File.separator)) p = p + File.separator;
                cutImage(srcImg, new java.io.FileOutputStream(p + srcImg.getName()), rect);
            } catch (FileNotFoundException e) {
            	System.out.println("the dest image is not exist.");
            }
        }else System.out.println("the dest image folder is not exist.");
    }
	
	public static void cutImage(File srcImg, OutputStream output, java.awt.Rectangle rect){
        if(srcImg.exists()){
            java.io.FileInputStream fis = null;
            ImageInputStream iis = null;
            try {
                fis = new FileInputStream(srcImg);
                
                String types = Arrays.toString(ImageIO.getReaderFormatNames()).replace("]", ",");
                String suffix = null;
                
                if(srcImg.getName().indexOf(".") > -1) {
                    suffix = srcImg.getName().substring(srcImg.getName().lastIndexOf(".") + 1);
                }
                if(suffix == null || types.toLowerCase().indexOf(suffix.toLowerCase()+",") < 0){
                    System.out.println("Sorry, the image suffix is illegal. the standard image suffix is {}." + types);
                    return ;
                }
                
                iis = ImageIO.createImageInputStream(fis);
                
                ImageReader reader = ImageIO.getImageReadersBySuffix(suffix).next();
                reader.setInput(iis,true);
                ImageReadParam param = reader.getDefaultReadParam();
                param.setSourceRegion(rect);
                BufferedImage bi = reader.read(0, param);
                ImageIO.write(bi, suffix, output);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if(fis != null) fis.close();
                    if(iis != null) iis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else {
            System.out.println("the src image is not exist.");
        }
    }

}
